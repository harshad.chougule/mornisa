<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Harshad Chogule
 * @version : 1.1
 * @since : 20 June 2018
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('user_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = PAGE_TITLE.' : Dashboard';

        if($this->session->userdata('isLoggedIn')&& $this->session->userdata('role')==1){
              $this->loadViews("dashboardAdmin", $this->global, NULL , NULL);
                }else if($this->session->userdata('isLoggedIn')&& $this->session->userdata('role')==2){
                      $this->loadViews("dashboardUser", $this->global, NULL , NULL);
                }else{
                    $this->load->view('login');
                }
        
        //$this->loadViews("dashboardAdmin", $this->global, NULL , NULL);
    }
    
   /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
     /**
     * This function is used to check whether email already exist or not
     */
    function checkCompanyExists()
    {
        $userId = $this->input->post("userId");
        $cname = $this->input->post("cname");

        if(empty($userId)){
            $result = $this->user_model->checkCompanyExists($cname);
        } else {
            $result = $this->user_model->checkCompanyExists($cname, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
   
     
    /**
     * This function is used to load the change password screen
     */
    function loadChangePass()
    {
        $this->global['pageTitle'] = PAGE_TITLE.' : Change Password';
        
        $this->loadViews("changePassword", $this->global, NULL, NULL);
    }
    
    
    /**
     * This function is used to change the password of the user
     */
    function changePassword()
    {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required|max_length[20]');
        $this->form_validation->set_rules('newPassword','New password','required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]|max_length[20]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->loadChangePass();
        }
        else
        {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            
            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password not correct');
                redirect('loadChangePass');
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
                
                if($result > 0) { $this->session->set_flashdata('success', 'Your password is updated, kindly use new password form next time.'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('loadChangePass');
            }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = PAGE_TITLE.' : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }


/*======================================== USER ===============================================================*/
 function Users()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $data['userRecords'] = $this->user_model->userListing();
            
            $this->global['pageTitle'] = PAGE_TITLE.' : User Listing';
            $this->global['controller'] ='user';
            $this->global['pagename'] ='users';             

            $this->loadViews("UsersMst", $this->global, $data, NULL);

        }
    }

     function addNew()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');

            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Add New User';

            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

/**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');            
            
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|xss_clean|max_length[128]');  
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
               // $name = ucwords(strtolower($this->input->post('fname')));
                $cname= $this->input->post('cname');
                $address = $this->input->post('address');
                $designation = $this->input->post('designation');

                $fname = $this->input->post('fname');
                $mname = $this->input->post('mname');
                $lname = $this->input->post('lname');
                $email = $this->input->post('email');
                //2 is default role id for user
                $roleId = 2;
                $mobile = $this->input->post('mobile');
                //default password is 123456
                $password = '123456';
                //Purchase order information
                $poNo = $this->input->post('poNo');
                $poDate = $this->input->post('poDate');
                $poValid = $this->input->post('poValid');
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'fname'=> $fname,'mname'=> $mname,'lname'=> $lname,'mobile'=>$mobile,'company_name'=>$cname,'address'=>$address, 'designation'=>$designation, 'POnumber'=>$poNo, 'POdate'=>$poDate, 'POvalidDate'=>$poValid, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New User created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('Users');
            }
        }
    }

    function editUserMst($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('Users');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Edit User';
            
            $this->loadViews("editUser", $this->global, $data, NULL);
        }
    }

 /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editUser($userId);
            }
            else
            {                
                $mobile = $this->input->post('mobile');
                $cname= $this->input->post('cname');
                $address = $this->input->post('address');
                $designation = $this->input->post('designation');
                $fname = $this->input->post('fname');
                $mname = $this->input->post('mname');
                $lname = $this->input->post('lname');
                 
                $userInfo = array();
                $userInfo = array('fname'=> $fname,'mname'=> $mname,'lname'=> $lname,'mobile'=>$mobile,'company_name'=>$cname,'address'=>$address, 'designation'=>$designation, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));           
                                 
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('Users');
            }
        }
    }

  /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteUser($userId, $userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

/*==================================================== USER ===============================================================*/


/*=================================================Category===============================================================*/
 function category()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $data['categoryRecords'] = $this->user_model->categoryListing();
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Category Listing';
            $this->global['controller'] ='user';
            $this->global['pagename'] ='category';
            
            $this->loadViews("categoryMst", $this->global, $data, NULL);

        }
    }

    function addCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data=0;
            $this->global['pageTitle'] = PAGE_TITLE.' : Add New Category';

            $this->loadViews("addNewCategory", $this->global, $data, NULL);
        }
    }

    function addNewCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('category_name','Category Name','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNewCategory();
            }
            else
            {
                $category_name = ucwords(strtolower($this->input->post('category_name')));
                
                $categoryInfo = array('category_name'=> $category_name, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewCategory($categoryInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Category created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category creation failed');
                }
                
                redirect('category');
            }
        }
    }

    function editCategoryMst($categoryId = NULL)
    {
        if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        }
        else
        {
            if($categoryId == null)
            {
                redirect('category');
            }
         
            $data['categoryInfo'] = $this->user_model->getCategoryInfo($categoryId);
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Edit Category';
            
            $this->loadViews("editCategory", $this->global, $data, NULL);
        }
    }


    function editCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $categoryId = $this->input->post('categoryId');
            
            $this->form_validation->set_rules('category_name','Category Name','trim|required|max_length[128]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editCategory($categoryId);
            }
            else
            {
                $category_name = ucwords(strtolower($this->input->post('category_name')));
                                
                $categoryInfo = array();
                
                $categoryInfo = array('category_name'=>ucwords($category_name), 'updatedBy'=>$this->vendorId,
                                 'updatedDtm'=>date('Y-m-d H:i:s'));
                              
                $result = $this->user_model->editCategory($categoryInfo, $categoryId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Category updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category updation failed');
                }
                
                redirect('category');
            }
        }
    }


     function deleteCategory()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $categoryId = $this->input->post('categoryId');
            $categoryInfo = array('isDeleted'=>1,'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->deleteCategory($categoryId, $categoryInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
/*=================================================Category===============================================================*/

    function adminCommonReport(){
        $this->load->helper('directory');
        $map = directory_map('./uploads/common');        
        $data['map'] = $map; 
        $data['report_name'] = $this->user_model->adminCommonReport();
        $this->global['pageTitle'] = PAGE_TITLE;            
        $this->loadViews("viewAdminCommonReport", $this->global, $data, NULL);
    }


/*==Upload Comman documents 
        By: Harshad
        On: 13/06-18        
*/
        /*
    **
     * This function is used to add new user to the system
     */
    function commonDocumentView()

    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = PAGE_TITLE.'Common Document ';
            $this->loadViews("commonDocumentView", $this->global, NULL, NULL);         
        }
    }


   /**
     * This function is used to upload image
     */
    function uploadCommonDocument()

    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $pathForImg = "common";
            //$date = str_replace( ':', '', $date);
            if (!is_dir('uploads/'.$pathForImg)) {
                mkdir('./uploads/' . $pathForImg, 0777, TRUE);
            }      
             // File upload configuration
                $uploadPath = './uploads/common/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'pdf';
                    
            // Load and initialize upload library
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
            // If file upload form submitted
            if(  !empty($_FILES['files']['name'])){
                $filesCount = count($_FILES['files']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['file']['name']     = $_FILES['files']['name'][$i];
                    $_FILES['file']['type']     = $_FILES['files']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                    $_FILES['file']['error']    = $_FILES['files']['error'][$i];
                    $_FILES['file']['size']     = $_FILES['files']['size'][$i];
                                    
                    // Upload file to server
                    if($this->upload->do_upload('file')){
                        // Uploaded file data                     
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");

                        $data['report_name']= $fileData['file_name'];
                        $data['upload_date']= date("Y-m-d");
                        $data['createdBy']= 1;
                        $data['createdDtm']= date("Y-m-d H:i:s");

                        $this->user_model->saveCommonReport($data);

                    }else{
                         
                       $error = array('error' => $this->upload->display_errors());
                        $errorMsg =$this->upload->display_errors();
                         $this->session->set_flashdata('error', $errorMsg);
                        redirect('commonDocumentView');
                    }
                }
                 $this->session->set_flashdata('success', 'Document publish successfully');
                 //redirect('commonDocumentView');
                 $this->user_model->sendEmailNotificationForCommonReport();
                  redirect('adminCommonReport');
                
            }

        }
    }



    function PO_validity_update()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
        
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $data['userRecords'] = $this->user_model->validityListing();
            
            $this->global['pageTitle'] = PAGE_TITLE.' : User Listing';
            $this->global['controller'] ='user';
            $this->global['pagename'] ='users';             

            $this->loadViews("validity_update", $this->global, $data, NULL);

        }
    }

    function updateValidity($userId = NULL)
    {
        if($this->isAdmin() == TRUE || $userId == 1)
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('PO_validity_update');
            }
            
            $data['userInfo'] = $this->user_model->getUserValidityInfo($userId);
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Edit User';
            
            $this->loadViews("updateValidityView", $this->global, $data, NULL);
        }
    }

     function renewValidity()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
                $userId = $this->input->post('userId');       
                $newpoNo = $this->input->post('newpoNo');
                $newpoDate= $this->input->post('newpoDate');
                $newpoValid = $this->input->post('newpoValid');
                 
                $userInfo = array();
                $userInfo = array('POnumber'=> $newpoNo,'POdate'=> $newpoDate,'POvalidDate'=> $newpoValid, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));           
                                 
                $result = $this->user_model->renewValidity($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Purchase Order information updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Purchase Order information updation failed');
                }
                
                redirect('PO_validity_update');
        }
    }
	
	/**
     * This function is used to check PO Validation
     */
    function checkPOValidy()
    {
        $poValid =new DateTime( $this->input->post("poValid"));
        $poDate = new DateTime($this->input->post("poDate"));

        if(($poValid) < ($poDate)){
            echo("false");
        }else{
            echo ("true");
        }
        
    }

    /**
     * This function is used to check PO Validation
     */
    function updatecheckPOValidy()
    {
        $poValid =new DateTime( $this->input->post("newpoValid"));
        $poDate = new DateTime($this->input->post("newpoDate"));

        if(($poValid) < ($poDate)){
            echo("false");
        }else{
            echo ("true");
        }
        
    }

}

?>