<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Report (UserReoprtController)
 * Report Class to control end user related report operations.
 * @author : Harshad
 * @version : 1.1
 * @since : 20 November 2018
 */
class Reports extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('report_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        
    }
    function viewAllOrders(){ 
            $searchText = $this->input->post('searchText');
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $data['userRecords'] = $this->report_model->reportListing();
            
            $this->global['pageTitle'] = PAGE_TITLE.' : Report Listing';
            $this->global['controller'] ='user';
            $this->global['pagename'] ='users';             

            $this->loadViews("viewAllOrders", $this->global, $data, NULL);
    }
    function viewOrder($pyamentId){ 
                
                $data['addressData'] = $this->report_model->getBillingAddress($pyamentId);
                $data['productData'] = $this->report_model->getProductInfo($pyamentId);
                $this->global['pageTitle'] = PAGE_TITLE.' : View Specific Report';
                $this->global['controller'] ='user';
                $this->global['pagename'] ='users';             

                $this->loadViews("viewOrder", $this->global, $data, NULL);
        }
        /*Fuction for update the order status*/
    function changeStatus(){ 
        $result = $this->report_model->changeStatus($this->input->post());
        echo(json_encode ($result)); 
    }   
}
