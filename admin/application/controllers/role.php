<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Role (RoleController)
 * Role class to control to create user roles
 * @author : Harshad Chogule
 * @version : 1.3
 * @since : 18 Feb 2019
 */
class Role extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('role_model');
        $this->isLoggedIn();   
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        
    }
    public function viewAllRoles(){
          if($this->isAdmin() == TRUE){
            $this->loadThis();
        }else{            
            $data['userRecords'] = $this->role_model->roleListing();            
            $this->global['pageTitle'] = PAGE_TITLE.' : Role Listing';
            $this->global['controller'] ='role';
            $this->global['pagename'] ='Role listing';             

            $this->loadViews("roleMst", $this->global, $data, NULL);

        }
    }

    public function addNewRole(){

        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('role_model'); 
            
            $this->global['pageTitle'] = PAGE_TITLE.' : add user role';

            $this->loadViews("addUserRole", $this->global, NULL, NULL);
        }
    }
}