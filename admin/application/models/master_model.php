<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model
{
    /** Start of Report Type Master **/  
    /**
     * This function is used to get the Report type masters
     * @param Null
     * @return data array : Result array 
     */
    function reportTypeListing(){
        $this->db->select("mst_report_type_id,mst_report_type_name");
        $this->db->from("mst_report_type");
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to add new report type master
     * @return number $insert_id : This is last inserted id
     */
    function addNewReportType($reportTypeInfo,$userId){
        $this->db->trans_start();
        $this->db->insert('mst_report_type', $reportTypeInfo);        
        $insert_id = $this->db->insert_id();
        if($insert_id){
            if($this->input->post('testParameter')){                  
                  foreach ($this->input->post('testParameter') as $value) {
                    $date1=date('Y-m-d H:i:s');
                    $data1[] = array(
                      'mst_report_type_id' => $insert_id ,
                      'mst_test_parameter_id' => $value ,
                      'createdBy'=>$userId, 
                      'createdDtm'=>$date1);
                  }
            
            $this->db->insert_batch('report_type_mapping', $data1); 
            } 
        }

        $this->db->trans_complete();        
        return $insert_id;
    }
    /**
     * This function is used to get the specific informamation about the report type master
     * @param {number} $reportId : This is report type master id (Table: mst_report_type)
     * @return {mixed} $result : This is searched result
     */
    function getReportTypeInfo($reportId = 0){
        $this->db->select("mst_report_type_id,mst_report_type_name");
        $this->db->from("mst_report_type");
        $this->db->where("isDeleted", 0);
        if($reportId != 0){
            $this->db->where("mst_report_type_id =", $reportId);
        }
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to update the Report type
     * @param number $reportTypeId : Current report type id
     * @param array $reportTypeInfo : Curent report info in the form off object/array
     * @return boolean $result : TRUE / FALSE
     */
    function updateReportType($reportTypeInfo,$reportTypeId,$userId){
        $this->db->trans_start();
       
        //delete the old record
        $this->db->where('mst_report_type_id', $reportTypeId);
        $this->db->delete('report_type_mapping'); 
         
        //update the new test parameter
        if($this->input->post('testParameter')){                  
                  foreach ($this->input->post('testParameter') as $value) {
                    $date1=date('Y-m-d H:i:s');
                    $data1[] = array(
                      'mst_report_type_id' => $reportTypeId ,
                      'mst_test_parameter_id' => $value ,
                      'createdBy'=>$userId, 
                      'createdDtm'=>$date1);
                  }
            
            $this->db->insert_batch('report_type_mapping', $data1); 
            } 
             
        //update the mater info
        $this->db->where('mst_report_type_id', $reportTypeId);
        $this->db->update('mst_report_type', $reportTypeInfo);
        if($this->db->trans_complete()){
            return true;
        }else{
            return FALSE;
        }
    }
     /** End of Report Type Master **/  
     /** Star of Nature of Sample **/

    /**
     * This function is used to get the Nature of sample listing
     * @param Null
     * @return data array : Result array 
     */
    function natureOfSampleListing(){
        $this->db->select("mst_nature_sample_id,mst_nature_sample_name,mst_nature_sample_code");
        $this->db->from("mst_nature_sample");
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to add new Nature of sample
     * @return number $insert_id : This is last inserted id
     */
    function addNewNatureOfSample($natureOfSampleInfo){
        $this->db->trans_start();
        $this->db->insert('mst_nature_sample', $natureOfSampleInfo);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete();        
        return $insert_id;
    }
    /**
     * This function is used to get the specific informamation about the nature of sample
     * @param {number} $reportId : This is Nature of sample id (Table: mst_nature_sample)
     * @return {mixed} $result : This is searched result
     */
    function getNatureOfSample($reportId = 0){
        $this->db->select("mst_nature_sample_id,mst_nature_sample_name,mst_nature_sample_code");
        $this->db->from("mst_nature_sample");
        $this->db->where("isDeleted", 0);
        if($reportId != 0){
            $this->db->where("mst_nature_sample_id =", $reportId);
        }
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to update the Nature of sample type
     * @param number $reportTypeId : Current report type id
     * @param array $reportTypeInfo : Curent report info in the form off object/array
     * @return boolean $result : TRUE / FALSE
     */
    function updateNatureOfSample($reportTypeInfo,$reportTypeId){
        $this->db->where('mst_nature_sample_id', $reportTypeId);
        $this->db->update('mst_nature_sample', $reportTypeInfo);
        
        return $this->db->affected_rows();
    }
     /** End of Nature of sample **/  
     /** Star of Type of Sample **/

    /**
     * This function is used to get the Type of sample listing
     * @param Null
     * @return data array : Result array 
     */
    function typeOfSampleListing(){
        $this->db->select("mst_type_sample_id,mst_type_sample_name,mst_type_sample_footer_note,mst_type_sample_limit,mst_type_sample_code");
        $this->db->from(" mst_type_sample");
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to add new Nature of sample
     * @return number $insert_id : This is last inserted id
     */
    function addNewTypeOfSample($typeOfSampleInfo){
        $this->db->trans_start();
        $this->db->insert('mst_type_sample', $typeOfSampleInfo);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete();        
        return $insert_id;
    }
    /**
     * This function is used to get the specific informamation about the nature of sample
     * @param {number} $reportId : This is Nature of sample id (Table: mst_nature_sample)
     * @return {mixed} $result : This is searched result
     */
    function getTypeOfSample($reportId = 0){
        $this->db->select("mst_type_sample_id,mst_type_sample_name,mst_type_sample_footer_note,mst_type_sample_limit,mst_type_sample_code");
        $this->db->from("mst_type_sample");
        $this->db->where("isDeleted", 0);
        if($reportId != 0){
            $this->db->where("mst_type_sample_id =", $reportId);
        }
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to update the Nature of sample type
     * @param number $reportTypeId : Current report type id
     * @param array $reportTypeInfo : Curent report info in the form off object/array
     * @return boolean $result : TRUE / FALSE
     */
    function updateTypeOfSample($reportTypeInfo,$reportTypeId){
        $this->db->where('mst_type_sample_id', $reportTypeId);
        $this->db->update('mst_type_sample', $reportTypeInfo);
        
        return $this->db->affected_rows();
    }
     /** End of Report Type Master **/ 
   









   /** Star of test parameter  **/

    /**
     * This function is used to get the test parameter listing
     * @param Null
     * @return data array : Result array 
     */
    function testParameterListing(){
        $this->db->select("mst_test_parameter_id,mst_test_parameter_name,mst_test_parameter_method,mst_test_parameter_unit,mst_test_parameter_limit");
        $this->db->from(" mst_test_parameter");
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to add new test parameter 
     * @return number $insert_id : This is last inserted id
     */
    function addNewTestParameter($testParameterInfo){
        $this->db->trans_start();
        $this->db->insert('mst_test_parameter', $testParameterInfo);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete();        
        return $insert_id;
    }
    /**
     * This function is used to get the specific informamation about the nature of sample
     * @param {number} $reportId : This is Nature of sample id (Table: mst_nature_sample)
     * @return {mixed} $result : This is searched result
     */
    function getTestParameter($reportId = 0){
        $this->db->select("mst_test_parameter_id,mst_test_parameter_name,mst_test_parameter_method,mst_test_parameter_unit,mst_test_parameter_limit");
        $this->db->from("mst_test_parameter");
        $this->db->where("isDeleted", 0);
        if($reportId != 0){
            $this->db->where("mst_test_parameter_id =", $reportId);
        }
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to update the test parameter 
     * @param number $reportTypeId : Current test parameter  id
     * @param array $reportTypeInfo : Curent test parameter  in the form off object/array
     * @return boolean $result : TRUE / FALSE
     */
    function updateTestParameter($reportTypeInfo,$reportTypeId){
        $this->db->where('mst_test_parameter_id', $reportTypeId);
        $this->db->update('mst_test_parameter', $reportTypeInfo);
        
        return $this->db->affected_rows();
    }
     /** End of Report Type Master **/ 



   /** Star of Sample type container  **/

    /**
     * This function is used to get the Sample type container
     * @param Null
     * @return data array : Result array 
     */
    function sampleContainerListing(){
        $this->db->select("mst_sample_container_id,mst_sample_container_name");
        $this->db->from(" mst_sample_container");
        $this->db->where("isDeleted", 0);
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to add new test parameter 
     * @return number $insert_id : This is last inserted id
     */
    function addNewSampleContainer($testParameterInfo){
        $this->db->trans_start();
        $this->db->insert('mst_test_parameter', $testParameterInfo);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete();        
        return $insert_id;
    }
    /**
     * This function is used to get the specific informamation about the nature of sample
     * @param {number} $reportId : This is Nature of sample id (Table: mst_nature_sample)
     * @return {mixed} $result : This is searched result
     */
    function getSampleContainer($reportId = 0){
        $this->db->select("mst_test_parameter_id,mst_test_parameter_name,mst_test_parameter_method,mst_test_parameter_unit,mst_test_parameter_limit");
        $this->db->from("mst_test_parameter");
        $this->db->where("isDeleted", 0);
        if($reportId != 0){
            $this->db->where("mst_test_parameter_id =", $reportId);
        }
        $query = $this->db->get();
        return $query->result();
    }
    /**
     * This function is used to update the test parameter 
     * @param number $reportTypeId : Current test parameter  id
     * @param array $reportTypeInfo : Curent test parameter  in the form off object/array
     * @return boolean $result : TRUE / FALSE
     */
    function updateSampleContainer($reportTypeInfo,$reportTypeId){
        $this->db->where('mst_test_parameter_id', $reportTypeId);
        $this->db->update('mst_test_parameter', $reportTypeInfo);
        
        return $this->db->affected_rows();
    }
     /** End of Report Type Master **/ 

    /**
     * This function is used to get the test parameter listing
     * @param Report type id
     * @return data array : Result array 
     */
    function testParameterListingWithReportTypeFlag($reportTypeId){
        return $this->db->query("select mst_test_parameter_id,mst_test_parameter_name,mst_test_parameter_method,mst_test_parameter_unit,isReportTypeMapped(mst_test_parameter_id,$reportTypeId) as flag ,mst_test_parameter_limit from mst_test_parameter where isDeleted =0")->result();         
    }
    
}
