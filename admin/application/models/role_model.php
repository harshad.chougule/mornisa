<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends CI_Model
{
    /**
     * This function is used to get the role listing
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function roleListing(){
    	$this->db->select('roleId,role,description');
        $this->db->from('tbl_roles');          
        $query = $this->db->get();        
        $result = $query->result_array();        
        return $result;

    }
}