<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Report_model extends CI_Model
{
    
 function reportListing()
    {
        return $this->db->query('SELECT mp.mst_payment_id,mp.userId, mp.total_price ,mp.payment_request_id ,mp.mojo_id ,mp.payment_status, u.email, u.name,u.mobile,mp.mst_order_status_id,mos.mst_order_name FROM mst_payment mp, tbl_users u, mst_order_status mos WHERE u.userId =mp.userId AND mos.mst_order_status_id = mp.mst_order_status_id ORDER BY mp.mst_payment_id DESC')->result_array();
        
    }
    /*Function to get report order
    */
    function getBillingAddress($paymentId){
    	return $this->db->query('SELECT mp.mst_payment_id,mp.userId ,tua.user_address, tua.user_pincode, tua.user_city,tua.shiping_note, tua.address_type, tua.user_name, tua.user_phone FROM mst_payment mp, tbl_user_address tua WHERE tua.user_id = mp.userId AND mp.mst_payment_id = '.$paymentId)->result_array();
    }
    /*Fucntion to get the product info*/
    function getProductInfo($paymentId){
    	return $this->db->query('SELECT tpt.payment_transaction_id, tpt.mst_payment_id, tpt.product_id, tpt.product_price, tpt.product_qty, tpt.total_price, tpt.createdDtm, mp.payment_request_id, mp.mojo_id, mp.payment_status, tp.product_id,tp.product_name,tp.product_sku, mos.mst_order_name, mos.mst_order_status_id FROM tbl_payment_transaction tpt, mst_payment mp, tbl_products tp, mst_order_status mos WHERE mp.mst_payment_id = tpt.mst_payment_id AND tp.product_id = tpt.product_id AND mp.mst_order_status_id =mos.mst_order_status_id AND mp.mst_payment_id  ='.$paymentId)->result_array();
    }
    /*Function to update the order status*/
    function changeStatus($data){
    	$this->db->set('mst_order_status_id',$data['status']);
		$this->db->where('mst_payment_id',$data['mst_payment_id']);
		if($this->db->update('mst_payment')){
			return true;
		}else{
			return false;
		}
    }
}