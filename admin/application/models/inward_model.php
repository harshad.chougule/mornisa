<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Inward_model extends CI_Model
{
     /**
     * This function is used to inward register listing
     * @param Null
     * @return data array : Result array 
     */
    function inwardRegisterListing($inward_register_id=0){
    	$this->db->select('*');
        $this->db->from('tbl_inward_register');
        $this->db->where("isDeleted", 0);
        if($inward_register_id){
            $this->db->where("inward_register_id", $inward_register_id);
        }
        $query = $this->db->get();        
        $result = $query->result_array();        
        return $result;
    }
    /**
     * This function is used to inward register listing
     * @param inward register data
     * @return Null
     */
    function inwardRegister1($data){
        $this->db->trans_start();
        $this->db->insert('tbl_inward_register', $data);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete();        
        return $insert_id;
    }
     /**
     * This function is used to update inward register listing
     * @param inward register data
     * @return Null
     */
    function updateInwardRegister1($data,$inward_register_id){
        $this->db->where('inward_register_id', $inward_register_id);
        $this->db->update('tbl_inward_register', $data);   
        if($this->db->affected_rows()){
           return $inward_register_id;
        }else{
            return false;
        }
    }

    /**
     * This function is used to get the test parameter listing
     * @param Report type id
     * @return data array : Result array 
     */
    function testParameterListing($reportTypeId){
        return $this->db->query("SELECT mtp.mst_test_parameter_id,mtp.mst_test_parameter_name,mtp.mst_test_parameter_method,mtp.mst_test_parameter_unit,mtp.mst_test_parameter_limit 
                    FROM mst_test_parameter mtp ,report_type_mapping rtm
                    WHERE rtm.mst_test_parameter_id = mtp.mst_test_parameter_id
                    AND mtp.isDeleted = 0
                    AND rtm.mst_report_type_id = $reportTypeId")->result_array();         
    }
/**
     * This function is used to get the test parameter listing
     * @param Report type id
     * @return data array : Result array 
     */
    function saveTestParameterForJobCard($jobCardData,$userId){

        if($jobCardData['testParameter']){
            $this->db->trans_start();
            //job card creation
                foreach ($jobCardData['testParameter'] as $value) {
                    $date1=date('Y-m-d H:i:s');
                    $data1[] = array(
                      'inward_register_id' => $jobCardData['inward_register_id'],
                      'mst_report_type_id' => $jobCardData['mst_report_type_id'],
                      'mst_test_parameter_id' => $value ,
                      'createdBy'=>$userId, 
                      'createdDtm'=>$date1);
                }            
            $this->db->insert_batch('tbl_job_card', $data1);
            //update tbl_inward_register - KUID and mst_report_type_id
            //KUID generation
            
           $shortCode = $this->db->query('SELECT mts.mst_type_sample_code,mns.mst_nature_sample_code FROM tbl_inward_register tir, mst_type_sample mts, mst_nature_sample mns where mts.mst_type_sample_id = tir.mst_type_sample_id and mns.mst_nature_sample_id = tir.mst_nature_sample_id and tir.inward_register_id = 5')->result();
           $kuidNumber = $this->db->query('SELECT KUID_number FROM  kuid_number where KUID_number_id=1')->result();

            $KUID = date('y').date('m').'/'.$shortCode[0]->mst_nature_sample_code.' '.$shortCode[0]->mst_type_sample_code.'/'.$kuidNumber[0]->KUID_number;

            $this->db->query('UPDATE `tbl_inward_register` SET `KUID`=?,`mst_report_type_id`=?  WHERE `inward_register_id`=?', array($KUID,$jobCardData['mst_report_type_id'],$jobCardData['inward_register_id']));

            //update new kuid number
            $newKuidNo =$kuidNumber[0]->KUID_number + 1; 
            $this->db->query('UPDATE `kuid_number` SET `KUID_number`=? WHERE `KUID_number_id`=1', array($newKuidNo));

            $this->db->trans_complete();
            return 1;             
        }else{
            return 0;
        }
    }

    function inwardRegisterListingForEdit($inward_register_id=0){
        return ($this->db->query('SELECT tir.* , tu.company_name, mns.mst_nature_sample_name,mts.mst_type_sample_name,msc.mst_sample_container_name FROM tbl_inward_register tir, tbl_users tu, mst_nature_sample mns,mst_type_sample mts, mst_sample_container msc WHERE tu.userId=tir.userId AND tir.isDeleted = 0 AND tir.mst_nature_sample_id = mns.mst_nature_sample_id AND tir.mst_type_sample_id = mts.mst_type_sample_id AND msc.mst_sample_container_id=tir.mst_sample_container_id AND tir.inward_register_id =?',$inward_register_id)->result_array());
       
    }
     
    /**
     * This function is used to ---
     * @param ---
     * @return data array : Result array 
     */
    function testParameterListingForEdit($reportTypeId,$inwardRegisterId){
        return $this->db->query("SELECT mtp.mst_test_parameter_id, selectTestParamenterForEdit(mtp.mst_test_parameter_id,$inwardRegisterId) as selectTestParamenterForEdit , mtp.mst_test_parameter_name,mtp.mst_test_parameter_method,mtp.mst_test_parameter_unit,mtp.mst_test_parameter_limit FROM mst_test_parameter mtp ,report_type_mapping rtm WHERE rtm.mst_test_parameter_id = mtp.mst_test_parameter_id AND mtp.isDeleted = 0 AND rtm.mst_report_type_id = $reportTypeId")->result_array();         
    }

    /**
     * This function is used to ---
     * @param ---
     * @return data array : Result array 
     */
    function getReportTypeInfoForEdit($inwardRegisterId){
        return $this->db->query("SELECT mrt.mst_report_type_id,mrt.mst_report_type_name FROM mst_report_type mrt, tbl_inward_register tir WHERE tir.mst_report_type_id = mrt.mst_report_type_id AND tir.inward_register_id  = $inwardRegisterId")->result_array();         
    }

}