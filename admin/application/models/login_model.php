<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('BaseTbl.userId, BaseTbl.password, BaseTbl.fname, BaseTbl.roleId, Roles.role');
        $this->db->from('tbl_admin_users as BaseTbl');
        $this->db->join('tbl_roles as Roles','Roles.roleId = BaseTbl.roleId');
        $this->db->where('BaseTbl.email', $email);
        $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->result();
        
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function used to check email exists or not
     * @param {string} $email : This is users email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_admin_users');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }


    /**
     * This function used to insert reset password data
     * @param {array} $data : This is reset password data
     * @return {boolean} $result : TRUE/FALSE
     */
    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to get customer information by email-id for forget password email
     * @param string $email : Email id of customer
     * @return object $result : Information of customer
     */
    function getCustomerInfoByEmail($email)
    {
        $this->db->select('userId, email, fname');
        $this->db->from('tbl_admin_users');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function used to check correct activation deatails for forget password.
     * @param string $email : Email id of user
     * @param string $activation_id : This is activation string
     */
    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows;
    }

    // This function used to create new password by reset link
    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }
     // This function used to create new password by reset link
    function resetPasswordEmail($data)
    {
        $randomPassword = rand();       

        $adminEmailId = INFO_EMAIL;
        $to = $data['email'];

                    $subject = "New Password for Mornisa Bio-Organics Pvt. Ltd.";

            $htmlContent = '
                <html>
                    <head>
                        <title>Welcome to Mornisa Bio-Organics Pvt. Ltd.</title>                   
                    </head>
                    <body>
                        <div>
                            <h2>Your new password is '.$randomPassword.'</h2>
                            <h4> Kindly reset the password for security </h4>               
                        </div>
                    </body>
                    <footer>
                        <p>Best Regards,</p>
                        <b>Mornisa Bio-Organics Pvt. Ltd.</b>
                    </footer>
                </html>';

            // Set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // Additional headers
            $headers .= 'From: '.INFO_EMAIL . "\r\n";
            //$headers .= 'Cc: harshad.r.chougule@gmail.com' . "\r\n";
            //$headers .= 'Bcc: welcome2@example.com' . "\r\n";



            // Send email
            if(mail($to,$subject,$htmlContent,$headers)):
                $this->db->where('email', $data['email']);
                $this->db->where('isDeleted', 0);
                //$this->db->where()
                $this->db->update('tbl_admin_users', array('password'=>getHashedPassword($randomPassword)));
               return 1;
            else:
                return 0;
               
            endif; 
        
    }
}

?>