<?php

$cat_info = $this->db->query('SELECT categoryId,category_name FROM `tbl_category` WHERE isDeleted = 0;')->result_array();
$user_info = $this->db->query('SELECT userId, company_name FROM `tbl_users` WHERE isDeleted = 0 AND roleId = 2;')->result_array();

 
?>
<!--  -->
<style type="text/css">
    
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> <?php echo PAGE_HEADER?>
        <small>Category Report View </small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
          
            <!-- left column -->
            <div class="col-md-offset-2 col-md-8">
              <!-- general form elements -->

                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Report Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                      <div class="box-body">
                     
                    <?php //echo form_open_multipart('reports/saveCategoryReport', 'id="regForm"', 'method="POST"');?>
                  <form id="viewCategoryForm">
                    <div class="row"> 
                     <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="user_email">Select Company Name</label>
                                         <select  class="form-control  "  name="user_email" id="user_email"   >
                                                 <?php foreach ($user_info as $row) { ?>
                                                      <option value="<?php echo $row['userId']; ?>">
                                                          <?php echo $row['company_name']; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>
                                </div>                     
                                <div class="col-md-3">                                
                                    <div class="form-group">
                                        <label for="year">Select Year</label>
                                             <select  class="form-control year "  name="year" id="year"   >
                                                 <?php foreach ($year as $row) { ?>
                                                      <option value="<?php echo $row; ?>">
                                                          <?php echo $row; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>                                    
                                </div>
                                <div class="col-md-3">                                
                                    <div class="form-group">
                                        <label for="fname">Select Month</label>
                                             <select  class="form-control month "  name="month" id="month"   >
                                                 <?php for ($m=1; $m<=12; $m++) { 
                                                    $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
                                                    $time = strtotime($month);
                                                    $newformat = date('n',$time);
                                                  ?>
                                                      <option value="<?php echo $newformat; ?>">
                                                          <?php echo $month; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>                                    
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="category_name">Select Category Name</label>
                                         <select  class="form-control  "  name="category_name" id="category_name"   >
                                                 <?php foreach ($cat_info as $row) { ?>
                                                      <option value="<?php echo $row['categoryId']; ?>">
                                                          <?php echo $row['category_name']; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>
                                </div>
                            </div>

                        <div class="row">                           
                        </div>
                    </div>
                
                        <div class="box-footer">
                             <div class="col-sm-12">
                                <div class="col-sm-6">
                                  <div class="alert alert-danger alert-dismissible" style="display: none;" id="d_alert">
                                     
                                    <strong>Sorry!</strong> No data available.
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                  <input type="button" onclick ="loadReports()" class="btn btn-success pull-right" value="Submit" />
                                </div>
                              </div>                            
                        </div>
                    </form>
              </div>
            </div>
            
        </div> <!-- Row ended-->
       
    </section>

    <section class="content">
        <div class="row">            
            <div class=" col-md-12">  
              <div class="box box-primary" >
                <div class="box-header">
               
                </div>
                <!-- /.card-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Sr. No.</th>
                      <th>File Name</th>
                      <th>Option</th>
                    </tr>
                    </thead>
                    <tbody> 
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Sr. No.</th>
                      <th>File Name</th>
                      <th>Option</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
                 
            </div>
          </div>
      </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/select2.full.min.js"></script>
<script type="text/javascript">
   jQuery(window).load(function (){   
      
    });
   
    
  $(document).ready(function() { 

    $('#example2').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
    
   $('.year').select2();
   $('.month').select2();
   $('#user_email').select2();
   $('#category_name').select2();
});

 function loadReports(){
      $.ajax({
      url:"<?php echo base_url();?>reports/getCategoryReportForAdmin",
      data:$('#viewCategoryForm').serialize(),
      type:'post',
      success: function(response){
        var jsonObj = JSON.parse(response);
        if(jsonObj.map){
          var t = $('#example2').DataTable();
          t.clear().draw();
           $("#d_alert").fadeOut('slow');
          if(jsonObj.map){
            for(i=0;i<jsonObj.map.length;i++){
                btnView  ='<a class="btn btn-sm btn-info" target="_blank" href='+jsonObj.path+jsonObj.map[i]+'><i class="fa fa-eye"></i></a>';
                fileName = jsonObj.map[i];
                 t.row.add( [
                        i+1,fileName,btnView
                    ] ).draw( false );
            }
          }
        }else{
          var t = $('#example2').DataTable();
          t.clear().draw();           
            $("#d_alert").fadeIn('slow');
            $("#d_alert").fadeOut(5000);
          }
        
       }
     });
  }
</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>