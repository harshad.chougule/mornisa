<?php //echo '<pre>',print_r($this->session->userdata('role')),exit;?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Admin Dashboard
        
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <?php $count=$this->db->query('SELECT count(userId) as count FROM tbl_users WHERE isDeleted=0')->row()->count;?>
                  <h3><?php echo $count; ?></h3>
                  <p>Customers</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person"></i>
                </div>
                <a href="<?php echo base_url(); ?>Users" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <?php $countcat=$this->db->query('SELECT count(product_id) as count FROM  tbl_products WHERE isDeleted=0')->row()->count;?>
                  <h3><?php echo $countcat; ?></h3>
                  <p>Total Product</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo base_url(); ?>viewAllOrders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <?php $countcatre=$this->db->query('SELECT SUM(total_price) as count FROM `mst_payment` where payment_status = 1')->row()->count;
                  if(!$countcatre){
                    $countcatre=0;
                  }
                  ?>
                  <h3><?php echo $countcatre; ?></h3>
                  <p>Total paid amount</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo base_url(); ?>viewAllOrders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <?php $countcomre=$this->db->query('SELECT count(mst_payment_id) as count FROM mst_payment')->row()->count;?>
                  <h3><?php echo $countcomre; ?></h3>
                  <p>Total transations</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?php echo base_url(); ?>viewAllOrders" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>
    </section>
</div>