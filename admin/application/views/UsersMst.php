<?php
        //echo '<pre>',print_r($userRecords),exit;
?>

<div class="content-wrapper">
<!-- DataTables -->
  <!-- <link rel="stylesheet" href="https://adminlte.io/themes/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> -->

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> <?php echo PAGE_HEADER?>
        <small> </small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <!-- <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNew"><i class="fa fa-plus"></i> Add New </a>
                </div> -->
            </div>
        </div>
        <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
              <div class="box box-primary">
                <div class="box-header">
                    <h2 class="box-title"><b>Customer List</b></h2>
                     
                </div><!-- /.box-header -->
                          
                                <div class="box-body table-responsive">
                                     <table class="table table-bordered table-striped datatable" id="table-2">
                                        <thead>
                                            <tr>
                                                <th>Sr. No.</th>
                                                <th>User Email</th>
                                                <th>Name</th>
                                                <th>Mobile</th>
                                                <th>Status</th>                 
                                                <th>Options</th> 
                                            </tr>
                                       </thead>
                                       <tbody>
                                        <?php $j=1; foreach ($userRecords as $row) {
                                           ?>
                                           <tr> 
                                              <td><?= $j; ?></td>
                                              <td><?= $row['email'];?></td>
                                              <td><?= $row['name']; ?></td>
                                              <td><?=$row['mobile'];?></td>
                                              <td>
                                                <?php if($row['status']){
                                                  echo '<span class="btn-success">Approved</span>';
                                                }else{
                                                  echo '<span class="btn-warning">Not Approved</span>';
                                                }?>
                                                  
                                                </td>
                                                <td>
                                                  <?php// echo base_url().'editUserMst/'.$row['userId']; ?>
                                                  <a class="btn btn-sm btn-info" href="#"><i class="fa fa-eye"></i></a>
                                                  
                                                </td>
                                           </tr>
                                           <?php $j++;} ?>
                                       </tbody>
                                    </table>
                          </div><!-- /.box-body -->
                    </div>
              </div><!-- /.box -->
        </div>
    </section>
</div>

<script type="text/javascript">
   jQuery(window).load(function ()
    {   
        var $ = jQuery;
          $("#table-2").dataTable();
       // Highlighted rows
        
    });

   jQuery(document).ready(function(){
  
  jQuery(document).on("click", ".deleteUser", function(){
    var userId = $(this).data("userid"),
      hitURL = baseURL + "deleteUser",
      currentRow = $(this);
    
    var confirmation = confirm("Are you sure to delete this user ?");
    
    if(confirmation)
    {
      jQuery.ajax({
      type : "POST",
      dataType : "json",
      url : hitURL,
      data : { userId : userId } 
      }).done(function(data){
        console.log(data);
        currentRow.parents('tr').remove();
        if(data.status = true) { alert("User successfully deleted"); }
        else if(data.status = false) { alert("User deletion failed"); }
        else { alert("Access denied..!"); }
      });
    }
  });
  
});

</script>
 
