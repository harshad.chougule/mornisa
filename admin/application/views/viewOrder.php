<?php $listOfStatus =  $this->db->query('SELECT * From mst_order_status  WHERE mst_order_status_id !=1')->result_array();?> 
<div class="content-wrapper">
 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> <?php echo PAGE_HEADER?>
        <small> </small>
      </h1>
    </section>
    <!-- Main content -->
    <section class="invoice">
      <div id="printDiv">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?=ORG_NAME;?>
            <small class="pull-right">Date: <?=date("d-m-Y");?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?=ORG_NAME;?></strong><br>
            <?=ADDRESS_LINE_ONE;?><br>
            <?=ADDRESS_LINE_TWO;?><br>
            <?=ADDRESS_LINE_THREE;?><br>
            Email: <?=ORG_EMAIL;?>
          </address>
        </div>
        <!-- /.col -->
          <input type="text" name="mst_payment_id" id="mst_payment_id" value="<?=($addressData[0]['mst_payment_id']);?>" hidden="hidden">
        <?php foreach ($addressData as $address){             
            if($address['address_type'] =='1'){ ?>
              <div class="col-sm-4 invoice-col"> 
              Billing To       
              <address>
                <strong><?=$address['user_name']?></strong><br>
                <?=$address['user_address']?><br>
                <?=$address['user_city']?> , <?=$address['user_pincode']?> <br>
                Phone : <?=$address['user_phone']?><br>                
              </address>
            </div>
            <?php
            }else{ ?>
              <div class="col-sm-4 invoice-col"> 
              Shipping To       
              <address>
                <strong><?=$address['user_name']?></strong><br>
                <?=$address['user_address']?><br>
                <?=$address['user_city']?> , <?=$address['user_pincode']?> <br>
                Phone : <?=$address['user_phone']?><br>                
              </address>
            </div>
              <?php
              $shipping_note = $address['shiping_note'];
            } ?>
           
          <?php } ?>
        <!-- /.col -->
     
        <!-- /.col -->
      </div>
      <!-- /.row -->
       <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
             
              <th>Product Name</th>
              <th>SKU</th>
              <th>Price</th>
              <th>QTY</th>
              <th>Total</th>
               
            </tr>
            </thead>            
            <tbody>

            <?php foreach($productData as $product){?>
            <tr>
              <td><?=$product['product_name']?></td>
              <td><?=$product['product_sku']?></td>
              <td>₹ <?=$product['product_price']?></td>
              <td><?=$product['product_qty']?></td>
              <td>₹ <?=$product['total_price']?></td>
            </tr>
             <?php
             $payment_request_id =$product['payment_request_id'];;
             $mojo_id = $product['mojo_id'];
             $order_date=$product['createdDtm'];
             $product_price=$product['product_price'];
             $product_qty=$product['product_qty'];
             $total_price=$product['total_price'];

              } ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Information:</p>
          
               <address>
                <strong> Transation ID: <?=$mojo_id?></strong><br>
                 Request ID: <?=$payment_request_id?><br>
                              
              </address>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
           Shipping Note:<br>
           <?=$shipping_note?>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Payment Date <?=date("d-m-Y", strtotime($order_date));?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>₹ <?=$product_price;?></td>
              </tr>
              <tr>
                <th>QTY</th>
                <td><?=$product_qty;?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>₹ <?=$total_price;?></td>
              </tr>
               
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#" onclick="printDiv('printDiv')" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" onclick="changeOrderStatus()" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Change the Delivery Status
          </button>         
          <strong> Order Delivery Status : </strong> <span class="btn-success"><?=$productData[0]['mst_order_name']?></span>
          <div class="col-md-2 pull-right">
          <select class=" form-control" id="statusID">            
            <?php
              foreach ($listOfStatus as $list) { ?>
                <option value="<?=$list['mst_order_status_id']?>"><?=$list['mst_order_name']?></option>
                
              <?php } ?>
          </select>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <script type="text/javascript">
    function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}



  function changeOrderStatus(){

    swal({
      title: "Are you sure?",
      text: "Are you sure that you want to change the status?",
      icon: "warning",
      dangerMode: true,
      buttons: [
        'No, cancel it!',
        'Yes, I am sure!'
      ],
    })
    .then(willDelete => {
      if (willDelete) {
        var hitURL = '<?php echo base_url()?>changeStatus';
        var status = $('#statusID').val();
        var mst_payment_id = $('#mst_payment_id').val();
        console.log(hitURL);
        console.log(status);
        console.log(mst_payment_id);
        jQuery.ajax({
        type : "POST",
        dataType : "json",
        url : hitURL,
        data : { 'status' : status,'mst_payment_id':mst_payment_id } 
        }).done(function(data){
          console.log(data);
         
          if(data.status = true) { 
            swal("Changed!", "Order status updated successfully :)", "success").then(function(){ 
               location.reload();
               }
            ); 
          }else { 
               swal("Error!", "Some things wents wrong please try again :(", "danger");
          }
        });
        
      }
    });


    /*var confirmation = confirm("Are you sure to delete this user ?");
    
    if(confirmation)
    {
      jQuery.ajax({
      type : "POST",
      dataType : "json",
      url : hitURL,
      data : { categoryId : categoryId } 
      }).done(function(data){
        console.log(data);
        currentRow.parents('tr').remove();
        if(data.status = true) { alert("Category successfully deleted"); }
        else if(data.status = false) { alert("Category deletion failed"); }
        else { alert("Access denied..!"); }
      });
    }
  }*/
   
   }

  </script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
