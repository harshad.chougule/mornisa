<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Customer Management
        <small>Add Customer</small>
      </h1>
    </section>
    
    <section class="content">    
        <div class="row">
            <!-- left column -->
            <div class="col-md-offset-2 col-md-8">
            <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Customer Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addUser" action="<?php echo base_url() ?>addNewUser" method="post" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="companyName">Name of the company </label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control required" id="cname" name="cname" maxlength="125">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">Address</label>
                                        <textarea  cols="40" rows="5"   class="form-control  " id="address"  name="address" maxlength="200" ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">First Name</label>
                                        <input type="text" class="form-control " id="fname" name="fname" maxlength="125">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mname">Middel Name</label>
                                        <input type="text" class="form-control  " id="mname"  name="mname" maxlength="125">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="lname">Last Name</label>
                                        <input type="text" class="form-control " id="lname" name="lname" maxlength="125">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Designation">Designation</label>
                                        <input type="text" class="form-control  " id="designation"  name="designation" maxlength="125">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mobile">Mobile Number</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control required digits" id="mobile" name="mobile" maxlength="10">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email address</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control required email" id="email"  name="email" maxlength="125">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="poNo">PO Number</label><span class="text-danger"> *</span>
                                        <input type="text" class="form-control required" id="poNo"  name="poNo" maxlength="50">
                                    </div>
                                </div>
                                 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="poDate">PO Number Date</label><span class="text-danger"> *</span>
                                        <input type="date" class="form-control required" id="poDate" name="poDate" onblur="compare()">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="poValid">PO Valid up to Date</label><span class="text-danger"> *</span>
                                        <input type="date" class="form-control required" id="poValid" name="poValid" onblur="compare()" >
                                    </div>
                                </div>
                                    
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>

<script type="text/javascript">
    

    function compare()
{
    var startDt = document.getElementById("poValid").value;
    var endDt = document.getElementById("poDate").value;
     $('#newpoNo-error').remove();

    if( (new Date(startDt).getTime() < new Date(endDt).getTime()))
    {
        $('#poValid').after('<label id="newpoNo-error" class="error" for="newpoNo">  PO valid upto date must be greater than PO number date.</label>')
        $('#poValid').attr('class','form-control required error');

         $(":submit").attr("disabled", true);
         
    }else{
        $('#poValid').attr('class','form-control required valid ');
        $('#newpoNo-error').remove();
        $(":submit").removeAttr("disabled");
    }
}
</script>