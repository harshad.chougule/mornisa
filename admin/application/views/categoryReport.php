<style type="text/css">
    .form-control-feedback {
    position: absolute;
    top: 8px;
    right: 8px;
}
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('assets/images/pageLoader.gif') 50% 50% no-repeat rgb(249,249,249);
    opacity: .8;
}
</style>
<?php
$user_info = $this->db->query('SELECT userId, company_name FROM `tbl_users`  
                                WHERE isDeleted = 0 and roleId =2')->result_array(); 
$cat_info = $this->db->query('SELECT categoryId,category_name FROM `tbl_category` WHERE isDeleted = 0;')->result_array();
 
?>
<!--  -->
<style type="text/css">
    
</style>
<div class="content-wrapper">
<div class="loader" style="display: none;"></div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> <?php echo PAGE_HEADER?>
        <small>Add Category Document View </small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-offset-2 col-md-8">
              <!-- general form elements -->
                  
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Report Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                      <div class="box-body">
                     
                    <?php echo form_open_multipart('reports/saveCategoryReport', 'id="regForm"', 'method="POST"');?>

                    <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="fname">Select Company Name</label>
                                             <select  class="form-control category_name " required="required" name="company_name" id="company_name"   >
                                                 <?php foreach ($user_info as $row) { ?>
                                                      <option value="<?php echo $row['userId']; ?>">
                                                          <?php echo $row['company_name']; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mname">Select Category Name</label>
                                         <select  class="form-control  "  name="category_name" id="category_name"   >
                                                 <?php foreach ($cat_info as $row) { ?>
                                                      <option value="<?php echo $row['categoryId']; ?>">
                                                          <?php echo $row['category_name']; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="month name">Select Month</label>
                                              <select  class="form-control month_number " required="required" name="month_number" id="month_number">
                                                 <?php for ($monthFlag = 0;$monthFlag< 12;$monthFlag++) { 
                                                    $tempMonthValue = $monthFlag +1;
                                                  ?>
                                                      <option value="<?php echo $tempMonthValue; ?>">
                                                          <?php echo (unserialize(MONTHS)[$monthFlag]); ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>                                    
                                </div>  
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="year">Select Year</label>
                                             <select  class="form-control year "  name="year" id="year"   >
                                                 <?php foreach (array_reverse($year) as $row) { ?>
                                                      <option value="<?php echo $row; ?>">
                                                          <?php echo $row; ?>
                                                      </option>
                                                  <?php } ?>
                                          </select>
                                    </div>
                                </div>

                            </div>

                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">  
                                  <div class="item form-group">
                                    <label class="control-label col-md-5 col-sm-5 col-xs-12" for="fileUpload">Click on choose file to upload category report :</label>
                                      <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                                       <input type="file" class="form-control has-feedback-left" id="files" name="files[]"  required="required" accept="application/pdf, application/vnd.ms-excel"   multiple="multiple" />
                                        <span class="fa fa-upload form-control-feedback left" aria-hidden="true"></span>
                                      </div>
                                  </div>
                            </div>
                          </div>
                        </div>
                    </div>
                
                        <div class="box-footer">
                            <input type="submit" class="btn btn-success pull-right" id="publish" value="Publish" />
                             
                            <input type="reset" class="btn btn-default" value="Reset" />
                        </div>
                    </form>
              </div>
            </div>
             <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $check_valid = $this->session->flashdata('check_valid');
                    if($check_valid)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('check_valid'); ?>
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div> <!-- Row ended-->
       
    </section>
    
</div>
<script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/select2.full.min.js"></script>
<script type="text/javascript">
   jQuery(window).load(function (){   
      
    });
   
    
  $(document).ready(function() { 
    $('.year').select2();
   $('.category_name').select2();
   $('#category_name').select2();
});
$("#publish").click(function(){
        //alert($('#files').val());
        if($('#files').val()){
            $(".loader").fadeIn("slow");
        }
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/addUser.js" type="text/javascript"></script>
