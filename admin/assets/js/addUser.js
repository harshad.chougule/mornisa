/**
 * File : addUser.js
 * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */

$(document).ready(function(){
	
	var addUserForm = $("#addUser");
	
	var validator = addUserForm.validate({
		
		rules:{
			 
			email : { required : true, email : true, remote : { url : baseURL + "checkEmailExists", type :"post"} },
			cname : { required : true, remote : { url : baseURL + "checkCompanyExists", type :"post"} },
			password : { required : true },
			cpassword : {required : true, equalTo: "#password"},
			mobile : { required : true, digits : true },
			role : { required : true, selected : true},
			poValid :{required:true, remote : { url : baseURL + "checkPOValidy", type:"post"} },
		},
		messages:{
			 
			email : { required : "This field is required", email : "Please enter valid email address", remote : "Email already taken" },
			cname : { required : "This field is required", remote : "Company name is already taken" },
			password : { required : "This field is required" },
			cpassword : {required : "This field is required", equalTo: "Please enter same password" },
			mobile : { required : "This field is required", digits : "Please enter numbers only" },
			role : { required : "This field is required", selected : "Please select atleast one option" },
			poValid :{ required : "This field is required ", remote : "PO valid upto date must be greater than PO number date" },			
		}
	});
});
