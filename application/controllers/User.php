<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Wlecome (index page)
 * 
 * @author : Harshad
 * @version : 1.1
 * @since : 26 July 2019
 */
class User extends BaseController {

	  public function __construct(){
        parent::__construct();
       // $this->isLoggedIn();   
       $this->load->model('user_model');
    }
	public function index(){
		if($this->session->userdata('isLoggedIn')){	
			$data['products'] = '';//$this->shop_model->get_product_info();
			 
			$this->global['pageTitle'] = PAGE_TITLE.' Dashboard';
			$this->loadViews('user/dashboard',$this->global,$data,NULL);
		}else{
			$this->global['pageTitle'] = PAGE_TITLE.' Login';
			$this->loadViews('user/login',$this->global,NULL,NULL);
		}
	}
	 
}
