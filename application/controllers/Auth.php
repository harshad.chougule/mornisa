<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
/**
 * Class : welcom (index page)
 * 
 * @author : Harshad
 * @version : 1.1
 * @since : 19 July 2019
 */
class auth extends BaseController {

	/**
     * This is default constructor of the class
     */
    public function __construct(){
        parent::__construct();
       // $this->isLoggedIn();   
        $this->load->model('auth_model');
    }
	 
	public function index(){
		// $this->login();
	}
	public function login(){
		$this->global['pageTitle'] = PAGE_TITLE.' Login | Signup';
		$this->loadViews('user/login',$this->global,NULL,NULL);
	}
	public function signup(){
		//OTP creation
			$digits = 4;
			$otp= rand(pow(10, $digits-1), pow(10, $digits)-1);		 
		//Data array creation	
		$signupInfo = array('name'=>$this->input->post('register-form-name'),'email'=>$this->input->post('register-form-email'), 'mobile'=>$this->input->post('register-form-phone'),'password'=>getHashedPassword($this->input->post('register-form-password')), 'createdDtm'=>date('Y-m-d H:i:s'),'otp'=>$otp);

		//check user is allready or not
			$isCheck=$this->auth_model->check_user_email($signupInfo['email']);
		  
			if($isCheck['isCheck']==1){
				//allready user but otp is not verified
				$this->session->set_flashdata('error', 'You are allready a user, but OTP is not verified. OTP is send to your mobile please check');				 
				$this->otp($isCheck['data'][0]);
			}elseif ($isCheck['isCheck']==2) {
				//allready verified user
				$this->session->set_flashdata('warning', 'Email id is allready a user');
				redirect('login');	
			}else{
				//signup
				$result=$this->auth_model->signup($signupInfo);
				//result return			  
	                if($result > 0){$this->session->set_flashdata('success', 'Your are one step a way. Please verify your mobile');
	                $data['user']=array('userId'=>$result);  
	                	}
	                else{$this->session->set_flashdata('error', 'Some thing went wrong!');}
	                
	                $object = new stdClass();
	                $object->userId = $result;
	                $this->otp($object);
			}
			
	}

	//OTP screen
	public function otp($data){
		$this->global['pageTitle'] = PAGE_TITLE.' OTP';
		$this->loadViews('user/otp',$this->global,$data,NULL);
	}
	//verify OTP
	public function verifyOTP(){
		 
		$result = $this->auth_model->verifyOTP($this->input->post());
		if($result['isCheck']==1){
			 $this->session->set_flashdata('success', 'Welcome to Mornisa! Please Login');
			 redirect('login');
		}else{
			$this->session->set_flashdata('error', 'Dear user, You enter invalid OTP');
			
			$object = new stdClass();
            $object->userId = $this->input->post('userId');
            $this->otp($object);
		}
	}
	//forgot passwrod
	function forgotpassword(){
		$email = $this->input->post('forgotPasswordEmail');
		//check user is allready or not
			$isCheck=$this->auth_model->check_user_email($email);

			if($isCheck['isCheck']==1){
				//allready user but otp is not verified
				$this->session->set_flashdata('warning', 'You are allready a user, but OTP is not verified. OTP is send to your mobile please check');
				$this->otp($isCheck['data'][0]);
			}elseif ($isCheck['isCheck']==2) {
				//allready verified user
				$this->load->helper('string');
                $data['email'] = $email;
                $data['activation_id'] = random_string('alnum',15);
                $data['createdDtm'] = date('Y-m-d H:i:s');
                $data['agent'] = getBrowserAgent();
                $data['client_ip'] = $this->input->ip_address();
                  
                        $data1["fname"] = $isCheck[0]->name;
                        $data1["email"] = $email;                        
                        $data1["message"] = "Reset Your Password";                     
                    $sendStatus = $this->auth_model->resetPasswordEmail($data1);

                    if($sendStatus){
                        $status = "send";
                      $this->session->set_flashdata('success', 'New re-password send to your email address, please check');
                    } else {
                       $this->session->set_flashdata('error', 'Reset password failed please try again');
                    }				
				redirect('login');	
			}else{
				$this->session->set_flashdata('error', 'Sorry you are not registered with US, please try to signup');
				redirect('login');	
			} 
	}


    /**
     * This function used to logged in user
     */
    public function loginMe()
    {        
            $email = $this->input->post('login-form-username');
            $password = $this->input->post('login-form-password');
            
            $result = $this->auth_model->loginMe($email, $password);           
            if(count($result) > 0)
            {
                foreach ($result as $res)
                {                    
                    $sessionArray = array('userId'=>$res->userId,
                                            'name'=>$res->name,
                                            'isLoggedIn' => TRUE
                                    );
                                    
                    $this->session->set_userdata($sessionArray);                    
                    $this->global['pageTitle'] = PAGE_TITLE;
		 			$this->loadViews('user/dashboard',$this->global,NULL,NULL);
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');                
                redirect('/login');
            }
        }

        /*
        Logout*/
        public function logout(){
		    $session_user=$this->session->userdata();
		    $unset_session = array(
		            'userId'    => '',		            
		            'name'     => '',
		            'isLoggedIn' => FALSE
		        );

		    $this->session->unset_userdata($unset_session);
		    $this->session->sess_destroy();		   
		    redirect('login');		  
		    } 

    }

