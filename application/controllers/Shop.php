<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
/**
 * Class : welcom (index page)
 * 
 * @author : Harshad
 * @version : 1.1
 * @since : 19 July 2019
 */
class Shop extends BaseController {

	  public function __construct(){
        parent::__construct();
       // $this->isLoggedIn();   
       $this->load->model('shop_model');
    }
	public function index()
	{
		
		$data['products'] = $this->shop_model->get_product_info();
		 
		$this->global['pageTitle'] = PAGE_TITLE.' Shop';
		$this->loadViews('shop/index',$this->global,$data,NULL);
	}
	public function demo(){
		print_r('demo');
	}
	public function viewProduct(){
		$this->global['pageTitle'] = PAGE_TITLE.' product';
		$this->loadViews('shop/viewProduct',$this->global,NULL,NULL);
	}
	public function checkout(){
		if($this->session->userdata('isLoggedIn') && ($this->input->post('quantity'))){
			$this->global['pageTitle'] = PAGE_TITLE.' Checkout';
		 
		$this->load->model('user_model');
		$data['user_info']=$this->user_model->get_user_info($this->session->userdata('userId'));
		$data['quantity']=$this->input->post('quantity');
		$data['product_info']=$this->shop_model->get_product_info($this->input->post('product_id'));
	 
		$this->loadViews('shop/checkout',$this->global,$data,NULL);
		}else{
			redirect('shop');
		}
		
	}
	public function placeOrder(){
		 
		if($this->session->userdata('isLoggedIn')){			  
			$this->load->model('user_model');
			$this->user_model->save_user_address($this->session->userdata('userId'));
			//get the product info
			$quantity=$this->input->post('quantity');
	        $product_price_array=$this->shop_model->get_product_price($this->input->post('product_id'));
	        $product_price = $product_price_array[0]['new_price'];
	        $total_price = $quantity * $product_price;
         	
         	$data['payment_info']=array('total_price' =>$total_price ,'user_email' =>$this->input->post('billing-form-email'),'phone'=>$this->input->post('billing-form-phone'),'product_name'=> $this->input->post('product_name'),'user_name'=>$this->input->post('billing-form-name'));

			$result = json_decode($this->checkPayment($data['payment_info']));
			
			if($result->success=='1'){
				//userId,payment_request_id,product_id,quantity,total_price,product_price
			 $transation_info = $this->shop_model->save_payment_info($this->session->userdata('userId'),$result->payment_request->id,$this->input->post('product_id'),$this->input->post('quantity'),$total_price,$product_price);
			 
			  header('Location: ' . $result->payment_request->longurl.'?transation_id='.$transation_info['payment_id']);

			}else{
				//error
				  
			}
			//
			 
			 
		}else{
			redirect('login');
		}
		
	}
	public function orderStatus(){
		//print_r($_GET['payment_id']);
		//payment_request_id,mojo_id
		$result = $this->shop_model->save_payment_status($_GET['payment_request_id'],$_GET['payment_id']);
		if($this->session->userdata('isLoggedIn')){
			$this->session->set_flashdata('success', 'Order successfully placed');
			redirect('dashboard');
		}else{
			$this->session->set_flashdata('success', 'Order successfully placed, Please relogin');
			redirect('login');
		}
		
	}	
	public function checkPayment($data){
		//$api = new Instamojo\Instamojo(API_KEY, AUTH_TOKEN);

		try {
					    
			$ch = curl_init();
			$redirect_url = base_url().'orderStatus';

			curl_setopt($ch, CURLOPT_URL, 'https://test.instamojo.com/api/1.1/payment-requests/');
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Api-Key:e67620151e2d46484772482a7894484b","X-Auth-Token:785ee24ccfab6782975600bf489073aa"));
			$payload = Array(
			    'purpose' => $data['product_name'],
			    'amount' =>  $data['total_price'],
			    'phone' => $data['phone'],
			    'buyer_name' => $data['user_name'],
			    'redirect_url' => $redirect_url,
			    'send_email' => true,			     
			    'send_sms' => true,
			    'email' => $data['user_email'],
			    'allow_repeated_payments' => false
			);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
			$response = curl_exec($ch);
			curl_close($ch); 

			return $response;
		}
		catch (Exception $e) {
		    print('Error: ' . $e->getMessage());
		}
	}
}
