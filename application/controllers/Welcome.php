<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
/**
 * Class : welcom (index page)
 * 
 * @author : Harshad
 * @version : 1.1
 * @since : 19 July 2019
 */
class Welcome extends BaseController {

	 
	public function index()
	{
		$this->global['pageTitle'] = PAGE_TITLE;
		 $this->loadViews('welcome_message',$this->global,NULL,NULL);
	}
	public function demo(){
		print_r('demo');
	}
}
