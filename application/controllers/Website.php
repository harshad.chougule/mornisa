<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
/**
 * Class : Website (website routing)
 * 
 * @author : Harshad
 * @version : 1.1
 * @since : 10 Aug 2019
 */
class Website extends BaseController {

  public function __construct(){
        parent::__construct();        
    }
	public function index(){
		
	}
	public function portfolio(){
		$this->global['pageTitle'] = PAGE_TITLE.' Portfolio';
		$this->loadViews('website/portfolio',$this->global,NULL,NULL);		
	}
	 
	public function portfolio_more(){
		$this->global['pageTitle'] = PAGE_TITLE.' Portfolio';
		$this->loadViews('website/portfolio_more',$this->global,NULL,NULL);		
	}
	public function gallery(){
		$this->global['pageTitle'] = PAGE_TITLE.' Gallery';
		$this->loadViews('website/gallery',$this->global,NULL,NULL);		
	}
	public function download(){
		$this->global['pageTitle'] = PAGE_TITLE.' Download';
		$this->loadViews('website/download',$this->global,NULL,NULL);		
	}
	public function contact(){
		$this->global['pageTitle'] = PAGE_TITLE.' Contact';
		$this->loadViews('website/contact',$this->global,NULL,NULL);		
	}
	public function sendAnEmail(){

		//
 
        $adminEmailId = INFO_EMAIL;
        $to = $_POST['template-contactform-email'];

        $subject = "You got new inquery";

            $htmlContent = '
                <html>
                    <head>
                        <title>New inquery for Mornisa</title>                   
                    </head>
                    <body>
                        <div>
                            <h4>Hello Admin,</h4>
                            <h4>You recived an email from contact page</h4>
                            <h4> Name: '.$_POST['template-contactform-name'].'</h4>
                            <h4> Email :'.$_POST['template-contactform-email'].'</h4>
                            <h4> Phone Number : '.$_POST['template-contactform-phone'].'</h4>
                            <h4> Subject : '.$_POST['template-contactform-subject'].'</h4>
                            <h4> Message : '.$_POST['template-contactform-message'].'</h4>
                                         
                        </div>
                    </body>
                    <footer>
                        <p>Best Regards,</p><br>
                        <b>Team Mornisa</b>
                    </footer>
                </html>';

            // Set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // Additional headers
            $headers .= 'From: '.INFO_EMAIL . "\r\n";
            //$headers .= 'Cc: harshad.r.chougule@gmail.com' . "\r\n";
            //$headers .= 'Bcc: welcome2@example.com' . "\r\n";

            // Send email
            if(mail($to,$subject,$htmlContent,$headers)){
            	echo true;
            }else{
            	echo flase;
            }
		 
	}
}
