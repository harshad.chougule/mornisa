<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model
{
	 

    /**
     * This function is used to get user info with billing and shiping address
     * @param userId: loged in user id
     * @return array: recourd set
     */
    function get_user_info($userId=0){
        $this->db->trans_start();
        //get user info
        $this->db->select('userId,email,name,mobile');
        $this->db->from('tbl_users');
        $this->db->where('isDeleted',0);
        $this->db->where('userId', $userId);
        
        $query = $this->db->get();
        $userArray= $query->result_array();
          //get user address
            $this->db->select('user_address_id,user_name,user_address,user_pincode,user_city,user_phone,address_type');
            $this->db->from('tbl_user_address');
            $this->db->where('user_id', $userId);      
           $query = $this->db->get();
          $userArray['address']=$query->result();
        $this->db->trans_complete();         
        return $userArray;
        
    }
/**
     * This function is used to save user billing and shiping address
     * @param array: shiping and billing address
     * @return null
     */
    function save_user_address($userId){
        $this->db->trans_start();
        //billing_addressCount
        $this->db->select('count(*) as billing_count');
        $this->db->from('tbl_user_address');
        $this->db->where('isDeleted',0);
        $this->db->where('address_type',1);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        $billing_count=$query->result_array();

        if(!$billing_count[0]['billing_count']){

            //Data array creation for billing address 
    $billing_address = array('user_id'=>$this->session->userdata('userId'),'user_name'=>$this->input->post('billing-form-name'), 'user_address'=>$this->input->post('billing-form-address'),'user_pincode'=>$this->input->post('billing-form-pin'),'user_city'=>$this->input->post('billing-form-city'),'user_phone'=>$this->input->post('billing-form-phone'), 'address_type'=>1,'createdDtm'=>date('Y-m-d H:i:s'),'createdBy'=>$userId);

        $this->db->insert('tbl_user_address', $billing_address);
        }else{
          //update address 
          $billing_address = array('user_name'=>$this->input->post('billing-form-name'), 'user_address'=>$this->input->post('billing-form-address'),'user_pincode'=>$this->input->post('billing-form-pin'),'user_city'=>$this->input->post('billing-form-city'),'user_phone'=>$this->input->post('billing-form-phone'),'updatedDtm'=>date('Y-m-d H:i:s'),'updatedBy'=>$userId);

          $this->db->where('user_id', $userId);
          $this->db->where('address_type', 1);
          $this->db->update('tbl_user_address', $billing_address);
        }
        //shipping_addressCount
        $this->db->select('count(*) as shipping_count');
        $this->db->from('tbl_user_address');
        $this->db->where('isDeleted',0);
        $this->db->where('address_type',2);
        $this->db->where('user_id', $userId);
        $query = $this->db->get();
        $shipping_count=$query->result_array();

        if(!$shipping_count[0]['shipping_count']){
          //Data array creation for billing address 
    $shipping_address = array('user_id'=>$this->session->userdata('userId'),'user_name'=>$this->input->post('shipping-form-name'), 'user_address'=>$this->input->post('shipping-form-address'),'user_pincode'=>$this->input->post('shipping-form-pin'),'user_city'=>$this->input->post('shipping-form-city'),'user_phone'=>$this->input->post('shipping-form-phone'),'shiping_note'=>$this->input->post('shipping-form-message'), 'address_type'=>2,'createdDtm'=>date('Y-m-d H:i:s'),'createdBy'=>$userId);

          $this->db->insert('tbl_user_address', $shipping_address);
        }else{
          //update address 

           $shipping_address = array('user_name'=>$this->input->post('shipping-form-name'), 'user_address'=>$this->input->post('shipping-form-address'),'user_pincode'=>$this->input->post('shipping-form-pin'),'user_city'=>$this->input->post('shipping-form-city'),'user_phone'=>$this->input->post('shipping-form-phone'),'shiping_note'=>$this->input->post('shipping-form-message'), 'address_type'=>2,'updatedDtm'=>date('Y-m-d H:i:s'),'updatedBy'=>$userId);

          $this->db->where('user_id', $userId);
          $this->db->where('address_type', 2);
          $this->db->update('tbl_user_address', $shipping_address);
        }
        $this->db->trans_complete(); 
    }

}