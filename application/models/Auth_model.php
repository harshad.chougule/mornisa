<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model
{
    /**
     * This function is used to get the role listing
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function signup($signupInfo){
    	$this->db->trans_start();
        $this->db->insert('tbl_users', $signupInfo);        
        $insert_id = $this->db->insert_id();       
        $this->db->trans_complete(); 
        $this->sendOTP($signupInfo);       
        return $insert_id;
    }
    function sendOTP($signupInfo){


        //Your authentication key
        $authKey = AUTH_TOKEN_MSG;

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $signupInfo['mobile'];

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = SENDER_ID_MSG;

        //Your message to send, Add URL encoding here.
        $messageText = 'Dear '.$signupInfo['name'].' , thank you for selecting Mornisa, to verify your account enter this OTP : '.$signupInfo['otp'];
        $message = urlencode($messageText);

        //Define route 
        $route = 4;
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="http://api.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);

        //Print error if any
        if(curl_errno($ch))
        {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);

       // echo $output;

    }
    /*Verify OTP*/
    function verifyOTP($data){
        
        $this->db->select('userId');
        $this->db->from('tbl_users');
        $this->db->where('userId =', $data['userId']);
        $this->db->where('otp =', $data['otp']);
        $query = $this->db->get();
         if(!($query->result())){
            //not a user
            return array('isCheck' => 0, 'data'=>$query->result());
        }else{
            $this->db->set('status',1);
            $this->db->where('userId',$data['userId']);
            $this->db->update('tbl_users');

            return array('isCheck' => 1, 'data'=>$query->result());
        }

    }
    function check_user_email($email){
        $this->db->select('userId,name,email,otp,status,mobile');
        $this->db->from('tbl_users');
        $this->db->where('email =', $email);
        $this->db->where('isDeleted =', 0);
        $query = $this->db->get();

        if(!($query->result())){
            //not a user
            return array('isCheck' => 0, 'data'=>$query->result());
        }else{
              if($query->result()[0]->status == 0){
                //otp is not verified

                $this->sendOTP($query->result_array()[0]);
               return array('isCheck' => 1, 'data'=>$query->result()); 
              }else{
                //verified user
                return array('isCheck' => 2, 'data'=>$query->result());
              }
        } 
          
    }

    // This function used to create new password by reset link
    function resetPasswordEmail($data)
    {
        $randomPassword = rand();       

        $adminEmailId = INFO_EMAIL;
        $to = $data['email'];

                    $subject = "New Password for Mornisa Bio-Organics Pvt. Ltd.";

            $htmlContent = '
                <html>
                    <head>
                        <title>Welcome to Mornisa Bio-Organics Pvt. Ltd.</title>                   
                    </head>
                    <body>
                        <div>
                            <h2>Your new password is '.$randomPassword.'</h2>
                            <h4> Kindly reset the password for security </h4>               
                        </div>
                    </body>
                    <footer>
                        <p>Best Regards,</p>
                        <b>Mornisa Bio-Organics Pvt. Ltd.</b>
                    </footer>
                </html>';

            // Set content-type header for sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            // Additional headers
            $headers .= 'From: '.INFO_EMAIL . "\r\n";
            //$headers .= 'Cc: harshad.r.chougule@gmail.com' . "\r\n";
            //$headers .= 'Bcc: welcome2@example.com' . "\r\n";



            // Send email
            if(mail($to,$subject,$htmlContent,$headers)):
                $this->db->where('email', $data['email']);
                $this->db->where('isDeleted', 0);
                //$this->db->where()
                $this->db->update('tbl_users', array('password'=>getHashedPassword($randomPassword)));
               return 1;
            else:
                return 0;
               
            endif; 
        
    }
     /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('userId,name,email,otp,status,mobile,password');
        $this->db->from('tbl_users ');         
        $this->db->where('isDeleted', 0);
        $this->db->where('email',$email);
        $query = $this->db->get();
        
        $user = $query->result();
        
        if(!empty($user)){
            if(verifyHashedPassword($password, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
}