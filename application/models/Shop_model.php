<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class shop_model extends CI_Model
{
	/**
     * This function is used to get porduct info
     * @param null
     * @return array: recourd set
     */
    function get_product_info($productId=0){
    	$this->db->trans_start();
        //get all the products
        $this->db->select('*');
        $this->db->from('tbl_products');
        $this->db->where('isDeleted',0);
        if($productId){
            $this->db->where('product_id',$productId);
        }
        $query = $this->db->get();
        $porductArray= $query->result_array();
         //get all the images
        for ($i=0;$i<sizeof($porductArray);$i++) {
	        $this->db->select('image.*');
	        $this->db->from('tbl_product_images as image');
	        $this->db->where('image.product_id', $porductArray[$i]['product_id']);      
	        $query = $this->db->get();
	        $porductArray[$i]['images']=$query->result();
		}
        $this->db->trans_complete(); 
		return $porductArray;
        /*$insert_id = $this->db->insert_id();       
               
        return $insert_id;*/
    }
/**
     * This function is used to get porduct info
     * @param $userId
     * @param $paymentRequestId
     * @param $productId
     * @param $quantity
     * @param $total_price
     * @return array: recourd set
     */
    function save_payment_info($userId,$paymentRequestId,$productId,$quantity,$total_price,$product_price){
        //$this->db->trans_start();
         
        //save mst_payment
        $mstPaymnet =array('userId' => $userId, 'payment_request_id' => $paymentRequestId,'createdBy'=>$userId,'createdDtm'=>date('Y-m-d H:i:s'),'total_price'=>$total_price);

        $this->db->insert('mst_payment', $mstPaymnet);
        $insert_id = $this->db->insert_id();
        
        //Save payment transation info

        $payment_transaction=array('mst_payment_id' => $insert_id, 'product_id' => $productId,'createdBy'=>$userId,'createdDtm'=>date('Y-m-d H:i:s'),'product_qty'=>$quantity,'product_price'=>$product_price,'total_price'=>$total_price);
        $this->db->insert('tbl_payment_transaction', $payment_transaction);

        $this->db->trans_complete(); 
        return array('total_price' => $total_price, 'payment_id' => $insert_id); 
       
    }

    /**
     * This function is used to get porduct price
     * @param product id
     * @return array: recourd set
     */
    function get_product_price($productId=0){         
        $this->db->select('product_id,new_price');
        $this->db->from('tbl_products');
        $this->db->where('isDeleted',0);        
        $this->db->where('product_id',$productId);
         
        $query = $this->db->get();
        return ($query->result_array());
    }
    /**
     * This function is used to get porduct price
     * @param payment_request_id
     * @param mojo_id
     * @return array: recourd set
     */
    function save_payment_status($payment_request_id,$mojo_id){ 
        $data = array('payment_status' =>1 ,'mojo_id'=>$mojo_id);        

        $this->db->where('payment_request_id', $payment_request_id);
        $this->db->update('mst_payment', $data);
        
        if($this->db->affected_rows()){
            return 1;
        }else{
            return 0;
        }
    }
}