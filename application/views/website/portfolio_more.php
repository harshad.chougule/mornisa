
		<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-parallax page-title-dark" style="padding: 250px 0; background-image: url('<?php echo base_url(); ?>assets/images/slider/swiper/1.png'); background-size: cover; background-position: center center;" data-bottom-top="background-position:0px 400px;" data-top-bottom="background-position:0px -500px;">

			<div class="container clearfix">
				<h1>About Us</h1>
				<span>Everything you need to know about our Company</span>
				 
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">
 
				<div class="row common-height clearfix">

					<div class="col-md-5 col-padding" style="background: url('images/team/3.jpg') center center no-repeat; background-size: cover;"></div>

					<div class="col-md-7 col-padding">
						<div>
							<div class="heading-block">
								<h3>Vision</h3>
							</div>
							<div class="row clearfix">
								<div class="col-lg-12">
									<p>"To provide pure, natural & quality Food Products to Customers who are trying to consume wholesome sugarcane sweeteners other than sugar". Our VISION is very clear as, we had seen various colors of jaggery in the market i.e. white, yellow, light orange & red. These colors of jaggery are the result of the use of elements which are not fit for / suitable for the health of end consumers. To come out of this adverse condition, we decided to come up with new technology which will make jaggery much suitable for human consumption in this era of health consciousness, stringent regulatory, legal compliances of the domestic and international market. Our prime MOTTO is to produce Natural Jaggery without chemicals used for specific color shade (say yellowish, golden, etc.) This jaggery will come up in Chocolate, dark brown color shade.</p>
									 
								</div>

								 

							</div>

						</div>
					</div>

				</div>

				<div class="row common-height bottommargin-lg clearfix">

					<div class="col-md-7 col-padding" style="background-color: #F5F5F5;">
						<div>
							<div class="heading-block">
								<!-- <span class="before-heading color">Developer &amp; Evangelist</span> -->
								<h3>Mission</h3>
							</div>

							<div class="row clearfix">

								<div class="col-lg-12">
									<p>"To become leading Manufacturer of Nutritional Jaggery Product". Our goal is, to become the manufacturer of world-class wholesome Jaggery products, like chunks, Powder, Bars, & Bhelies, To Cater our clients a quality range of products, we have a diligent, competent team of quality controllers which tests each batch of our products minutely on various quality parameters. All our products are rich in all essential nutrients that are required in a balanced diet than it cane sugar-based sweeteners.</p>
									   
								</div>
 

							</div>

						</div>
					</div>

					<div class="col-md-5 col-padding" style="background: url('images/team/8.jpg') center center no-repeat; background-size: cover;"></div>

				</div>

				<div class="container clearfix">

					<div class="clear"></div>

					<div class="heading-block center">
						<h4>Our Strengths</h4>
					</div>

					<ul class="clients-grid grid-3 nobottommargin clearfix">
					<li>No BOILER. Zero Discharge Unit</li> 
					<li>It's an Environmental-friendly Factory, as we use Green energy.</li>
					<li>No by-products like Molasses or Press Mud. This flagship Technology is used first time in India & is well protected intellectually.</li> 
					<li>Lastly, our vision is to offer & invest in new dimensions & technology as a leader to jaggery industry of new generation demanding high standards of food safety as well as social responsibility, occupational safety, and environmental management, as consumers are running away from only sugar-based sweetening options</li>
					<li>This project is surely Socio economy friendly venture</li>
					<li>There is NO question of any type of Adulteration in our product. Natural jaggery is beneficial to consumers in 14 different WAY.</li> 
					</ul>

				</div>
 

			</div>

		</section><!-- #content end -->