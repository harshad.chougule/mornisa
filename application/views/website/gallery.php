
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Gallery</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Gallery</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Portfolio Filter
					============================================= -->
					<ul id="portfolio-filter" class="portfolio-filter clearfix">

						<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
						<li><a href="#" data-filter=".pf-icons">Type 1</a></li>
						<li><a href="#" data-filter=".pf-illustrations">Type 2</a></li>
						<li><a href="#" data-filter=".pf-uielements">Type 3</a></li>
						<li><a href="#" data-filter=".pf-media">Type 4</a></li>
						<li><a href="#" data-filter=".pf-graphics">Type 5</a></li>

					</ul><!-- #portfolio-filter end -->

					<div class="clear"></div>

					<div id="demo-gallery" class="masonry-thumbs grid-3" data-big="2" data-lightbox="gallery">
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-1.jpg" data-lightbox="gallery-item" class="pf-icons pf-media"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-2.jpg" alt="Gallery Thumb 1"></a>
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-2.jpg" data-lightbox="gallery-item" class="pf-illustrations pf-uielements"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-2.jpg" alt="Gallery Thumb 2"></a>
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-3.jpg" data-lightbox="gallery-item" class="pf-uielements pf-graphics"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-3.jpg" alt="Gallery Thumb 3"></a>
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-4.jpg" data-lightbox="gallery-item" class="pf-media pf-illustrations"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-4.jpg" alt="Gallery Thumb 4"></a>
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-4.jpg" data-lightbox="gallery-item" class="pf-graphics"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-5.jpg" alt="Gallery Thumb 5"></a>
						<a href="<?php echo base_url(); ?>assets/images/gallery/plant-6.jpg" data-lightbox="gallery-item" class="pf-icons pf-uielements"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/gallery/plant-6.jpg" alt="Gallery Thumb 6"></a>
					</div>

				</div>

			</div>

		</section><!-- #content end -->

	<script>

		jQuery(window).on( 'load', function(){

			var $container = $('#demo-gallery');
			$('#portfolio-filter a').click(function(){
				$('#portfolio-filter li').removeClass('activeFilter');
				$(this).parent('li').addClass('activeFilter');
				var selector = $(this).attr('data-filter');
				$container.isotope({ filter: selector });
				return false;
			});

		});


	</script>