<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Contact</h1>
				<span>Fell free to Contact us</span>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Contact</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Google Map
		============================================= -->
		<section id="google-map" class="gmap slider-parallax">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d59808.12208055812!2d74.97293766271075!3d20.46487979432504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bd9550deb6573f5%3A0xb40712816fc972bb!2sChalisgaon%2C+Maharashtra+424101!5e0!3m2!1sen!2sin!4v1524823961024"  height="450" frameborder="0" style="border:0; width: 100%" allowfullscreen=""></iframe>
		</section>
		<!-- Google map end -->

<section id="content" style="margin-bottom: 0px;">

			<div class="content-wrap">

				<div class="container clearfix">

					<!--  -->
					<div class="col-md-12">
			                <?php
			                    $this->load->helper('form');
			                    $error = $this->session->flashdata('error');
			                    if($error)
			                    {
			                ?>
			                <div class="alert alert-danger alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('error'); ?>                    
			                </div>
			                <?php } ?>
			                <?php  
			                    $success = $this->session->flashdata('success');
			                    if($success)
			                    {
			                ?>
			                <div class="alert alert-success alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('success'); ?>
			                </div>
			                <?php } ?>
			                

			                <?php  
			                    $warning = $this->session->flashdata('warning');
			                    if($warning)
			                    {
			                ?>
			                 <div class="alert alert-warning alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('warning'); ?>
			                </div>
			                <?php } ?>


			                <div class="row">
			                    <div class="col-md-12">
			                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
			                    </div>
			                </div>
       					 </div>
					<!--  -->
					<!-- Postcontent
					============================================= -->
					<div class="postcontent nobottommargin">

						<h3>Send us an Email</h3>

						<div class="contact-widget">


							<form class="nobottommargin" action="<?php echo base_url();?>website/sendAnEmail" method="post" id="contactForm" >

								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactform-name">Name <small>*</small></label>
									<input type="text" id="template-contactform-name" name="template-contactform-name" value="" class="sm-form-control required">
								</div>

								<div class="col_one_third">
									<label for="template-contactform-email">Email <small>*</small></label>
									<input type="email" id="template-contactform-email" name="template-contactform-email" value="" class="required email sm-form-control">
								</div>

								<div class="col_one_third col_last">
									<label for="template-contactform-phone">Phone</label>
									<input type="text" id="template-contactform-phone" name="template-contactform-phone" value="" class="sm-form-control">
								</div>

								<div class="clear"></div>

								<div class="col_two_third">
									<label for="template-contactform-subject">Subject <small>*</small></label>
									<input type="text" id="template-contactform-subject" name="template-contactform-subject" value="" class="required sm-form-control">
								</div>

								 
								<div class="clear"></div>

								<div class="col_full">
									<label for="template-contactform-message">Message <small>*</small></label>
									<textarea class="required sm-form-control" id="template-contactform-message" name="template-contactform-message" rows="6" cols="30"></textarea>
								</div>

								<div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control">
								</div>

								<div class="col_full">
									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit" onClick="sendAnEmail()">Send Message</button>
								</div>

							</form>
						</div>

					</div><!-- .postcontent end -->

					<!-- Sidebar
					============================================= -->
					<div class="sidebar col_last nobottommargin">

						<address>
							<strong>Headquarters:</strong><br>						 
								C-46, MIDC, KHADKI, CHALISGAON,
								JALGAON - 424101<br>
								MAHARSHTRA, <br>INDIA.<br>
						</address>
						<abbr title="Phone Number"><strong>Phone:</strong></abbr> +(91) 9404494440<br>
						<abbr title="Fax"><strong>Fax:</strong></abbr> +(91) 9404494477<br>
						<abbr title="Email Address"><strong>Email:</strong></abbr> info@mornisa.com
 
						<div class="widget noborder notoppadding">

							<a href="https://www.facebook.com/mornisajaggery/" class="social-icon si-small si-dark si-facebook">
								<i class="icon-facebook"></i>
								<i class="icon-facebook"></i>
							</a>

							<a href="https://www.instagram.com/mornisajaggery/" class="social-icon si-small si-dark si-instagram">
								<i class="icon-instagram"></i>
								<i class="icon-instagram"></i>
							</a>

							<a href="https://www.youtube.com/channel/UCNNTyd2hzKE134KlUImE8Rg" class="social-icon si-small si-dark si-youtube">
								<i class="icon-youtube"></i>
								<i class="icon-youtube"></i>
							</a>
 

						</div>

					</div><!-- .sidebar end -->

				</div>

			</div>

		</section>
  <script type="text/javascript">

  	function sendAnEmail(){
  	
  		if ($('#template-contactform-name').val()=='' || $('#template-contactform-email').val()=='' ||$('#template-contactform-phone').val()=='' || $('#template-contactform-subject').val()=='' || $('#template-contactform-message').val()==''){
  			return false;
  		} 
  		
  		var formData = $("#contactForm").serialize(); 
  		  $.ajax({
            url:"<?php echo base_url();?>website/sendAnEmail",
            data:formData,
            type:'POST',
            success: function(response){
            	console.log(response);
            	
            notifyMsg = 'harshad';
             /* var jsonObj = JSON.parse(response);
              var html='';
               console.log(jsonObj);*/
               toastr.success(notifyMsg);
                 
             }
           });//end of #report
  	}
  </script>