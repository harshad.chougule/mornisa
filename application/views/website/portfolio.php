	<!-- Content
		============================================= -->
		<section id="">

			<div class="content-wrap">

				<div class="container clearfix">

					 

					<div class="col_one">
						<div class="feature-box fbox-small fbox-plain" data-animate="fadeIn">
							<div class="fbox-icon">
								<a href="#"><i class="icon-line2-book-open"></i></a>
							</div>
							<h3>Introduction</h3>
							<h4>Established in the year 2015, We "Mornisa Bio-Organics Pvt. Ltd.," are amongst the prominent Processors and Manufacturer of a comprehensive range of Jaggery (Conical typical Bhelies, Cake, Powder, and Syrup) Products. It is a Dream project of Energetic Promotors, entrepreneurs with the vision to develop the local sourcing area of sugarcane in Girana River Basin. This project we have erected by taking motivation from our Honorable Prime Minister, Chief Minister of Maharashtra, they are supporting clean & green technologies state of the art technology under make in India mission. Our Sole Aim to bring this project to Chalisgaon MIDC with huge investment is that we are natives of this region. This project will give employment to nearly 1000 families (Direct or In-Direct) of the area to stop the plight of farmers of the region. There are nearly 7 lacs. MT Sugarcane is grown & there is no Sugar or Jaggery factory in the vicinity, which could be a great support to sugarcane producers and the community. Considering this and another fact, it was decided to set up a jaggery unit with modern technology and the sustained capacity.</h4>
						</div>
					</div>
 
					<div class="clear"></div>

				</div>

				<div class="clear"></div>

				 

				<div class="section notopmargin noborder nobottommargin" >
				
					 <div class="clear"></div>

				</div>
 

				 

				<div class="row bottommargin-lg common-height">

					<div class="col-lg-4 dark col-padding ohidden" style="background-color: #73b92c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">Why choose Us</h3>
							<p style="line-height: 1.8;">1. No BOILER. Zero Discharge Unit <br>
								2. It's an Environmental-friendly Factory, as we use Green energy.<br>
								3. No by-products like Molasses or Press Mud. This flagship Technology is used first time in India & is well protected intellectually.
								.</p>
							<a href="<?php echo base_url()?>portfolio_more" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
							<i class="icon-bulb bgicon"></i>
						</div>
					</div>

					<div class="col-lg-4 dark col-padding ohidden" style="background-color: #50a939;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">Our Mission</h3>
							<p style="line-height: 1.8;">"To provide pure, natural & quality Food Products to Customers who are trying to consume wholesome sugarcane sweeteners other than sugar". Our VISION is very clear as, we had seen various colors of jaggery in the market i.e. white, yellow, light orange & red.</p>
							<a href="<?php echo base_url()?>portfolio_more" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
							<i class="icon-cog bgicon"></i>
						</div>
					</div>

					<div class="col-lg-4 dark col-padding ohidden" style="background-color: #499c44;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">Vision</h3>
							<p style="line-height: 1.8;">"To become leading Manufacturer of Nutritional Jaggery Product". Our goal is, to become the manufacturer of world-class wholesome Jaggery products, like chunks, Powder, Bars, & Bhelies, To Cater our clients a quality range of products</p>
							<a href="<?php echo base_url()?>portfolio_more" class="button button-border button-light button-rounded uppercase nomargin">Read More</a>
							<i class="icon-thumbs-up bgicon"></i>
						</div>
					</div>

					<div class="clear"></div>

				</div>

				
 

			 


			</div>

		</section><!-- #content end -->