<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Download</h1>
				<span>Our Latest Download Section</span>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?=base_url()?>">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Blog</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Posts
					============================================= -->
					<div id="posts" class="post-grid grid-container clearfix" data-layout="fitRows">

					 

						 

						<div class="entry clearfix">
							<div class="entry-image">
								<div class="fslider" data-arrows="false" data-lightbox="gallery">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide"><a href="<?php echo base_url();?>uploads/brochure/Mornisa-Brochure.pdf#toolbar=0" width="500" height="375"  target="_blank"  ><img  src="<?php echo base_url(); ?>assets/images/blog/grid/10.jpg" alt="Standard Post with Gallery"></a></div>
											 
										</div>
									</div>
								</div>
							</div>
							<div class="entry-title">
								<h2><a href="<?php echo base_url();?>uploads/brochure/Mornisa-Brochure.pdf#toolbar=0" width="500" height="375"  target="_blank" >Mornisa Brochure</a></h2>
							</div>
						  
						</div>

						<div class="entry clearfix">
							<div class="entry-image">
								<div class="fslider" data-arrows="false" data-lightbox="gallery">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide"><a href="<?php echo base_url();?>uploads/brochure/How-to-Make-Tea.pdf#toolbar=0" width="500" height="375"  target="_blank"  ><img  src="<?php echo base_url(); ?>assets/images/blog/grid/10.jpg" alt="Standard Post with Gallery"></a></div>
											 
										</div>
									</div>
								</div>
							</div>
							<div class="entry-title">
								<h2><a href="<?php echo base_url();?>uploads/brochure/How-to-Make-Tea.pdf#toolbar=0" width="500" height="375"  target="_blank" >How To Make Tea</a></h2>
							</div>
						  
						</div>

						<div class="entry clearfix">
							<div class="entry-image">
								<div class="fslider" data-arrows="false" data-lightbox="gallery">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide"><a href="<?php echo base_url();?>uploads/brochure/1-Fold-Brochure-English.pdf#toolbar=0" width="500" height="375"  target="_blank"  ><img  src="<?php echo base_url(); ?>assets/images/blog/grid/10.jpg" alt="Standard Post with Gallery"></a></div>
											 
										</div>
									</div>
								</div>
							</div>
							<div class="entry-title">
								<h2><a href="<?php echo base_url();?>uploads/brochure/1-Fold-Brochure-English.pdf#toolbar=0" width="500" height="375"  target="_blank" >Fold Brochure part 1</a></h2>
							</div>
						  
						</div>
 						<div class="entry clearfix">
							<div class="entry-image">
								<div class="fslider" data-arrows="false" data-lightbox="gallery">
									<div class="flexslider">
										<div class="slider-wrap">
											<div class="slide"><a href="<?php echo base_url();?>uploads/brochure/2-Fold-Brochure.pdf#toolbar=0" width="500" height="375"  target="_blank"  ><img  src="<?php echo base_url(); ?>assets/images/blog/grid/10.jpg" alt="Standard Post with Gallery"></a></div>
											 
										</div>
									</div>
								</div>
							</div>
							<div class="entry-title">
								<h2><a href="<?php echo base_url();?>uploads/brochure/2-Fold-Brochure.pdf#toolbar=0" width="500" height="375"  target="_blank" >Fold Brochure part 1</a></h2>
							</div>
						  
						</div>
  
 

					</div><!-- #posts end -->

					<!-- Pagination
					============================================= -->
				 

				</div>

			</div>

		</section><!-- #content end -->