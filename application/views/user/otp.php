<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>My Account</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Login</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					 

					 

					<div class="col_two_third col_last nobottommargin">
						<div class="col-md-12">
			                <?php
			                    $this->load->helper('form');
			                    $error = $this->session->flashdata('error');
			                    if($error)
			                    {
			                ?>
			                <div class="alert alert-danger alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('error'); ?>                    
			                </div>
			                <?php } ?>
			                <?php  
			                    $success = $this->session->flashdata('success');
			                    if($success)
			                    {
			                ?>
			                <div class="alert alert-success alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('success'); ?>
			                </div>
			                <?php } ?>
			                

			                <?php  
			                    $warning = $this->session->flashdata('warning');
			                    if($warning)
			                    {
			                ?>
			                 <div class="alert alert-warning alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('warning'); ?>
			                </div>
			                <?php } ?>


			                <div class="row">
			                    <div class="col-md-12">
			                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
			                    </div>
			                </div>
       					 </div>
       					</div>
   					 
				<div class="col_two_third col_last nobottommargin" id="otpView">

						<h3>Verify your acccount. Register Now.</h3>

						 
						<!-- <?php echo base_url();?>verifyOTP -->
						<form id="register-form" name="register-form" class="nobottommargin" action="<?php echo base_url();?>verifyOTP" method="post">
							<div class="col_half">
								<label for="OTP">OTP:</label>
								<input type="text" id="otp" required="required" name="otp" value="" class="form-control" />
								 
								<input type="text" name="userId" value="<?=$userId?>" hidden="hidden">
							</div> 

							<div class="clear"></div>

							<div class="col_full ">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
							</div>

						</form>

					</div>

				</div>

			</div>

		</section><!-- #content end -->


		<script type="text/javascript">
			$(document).ready(function() {c

			});

			 
			 
 

		</script>