<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>My Account</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Pages</a></li>
					<li class="breadcrumb-item active" aria-current="page">Login</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_one_third nobottommargin">
						<div class="well well-lg nobottommargin" id="login">
							<form id="login-form" name="login-form" class="nobottommargin" action="<?php echo base_url();?>auth/loginMe" method="post">
								<h3>Login to your Account</h3>
								<div class="col_full">
									<label for="login-form-username">Email Address:</label>
									<input type="text" required="required" id="login-form-username" name="login-form-username" value="" class="form-control" />
								</div>
								<div class="col_full">
									<label for="login-form-password">Password:</label>
									<input type="password" required="required" id="login-form-password" name="login-form-password" value="" class="form-control" />
								</div>
								<div class="col_full nobottommargin">
									<button class="button button-3d nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
									<a href="#"onclick="forgotPassView();" class="fright">Forgot Password?</a>
								</div>
							</form>
						</div>
						<!-- Forgot password -->
						<div class="well well-lg nobottommargin" id="forgotPassword">
							<form id="login-form" name="login-form" class="nobottommargin" action="<?php echo base_url();?>auth/forgotpassword" method="post">
								<h3>Forgot password?</h3>
								<div class="col_full">
									<label for="login-form-username">Enter your email:</label>
									<input type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"  required="required" id="forgotPasswordEmail" name="forgotPasswordEmail" value="" class="form-control" />
								</div>								
								<div class="col_full nobottommargin">
									<button class="button button-3d nomargin" id="login-form-submit" name="login-form-submit" value="login">Submit</button>
									<a href="#" onclick="loginView()" class="fright">Login?</a>
								</div>
							</form>
						</div>
					</div>

					 

					<div class="col_two_third col_last nobottommargin">
						<div class="col-md-12">
			                <?php
			                    $this->load->helper('form');
			                    $error = $this->session->flashdata('error');
			                    if($error)
			                    {
			                ?>
			                <div class="alert alert-danger alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('error'); ?>                    
			                </div>
			                <?php } ?>
			                <?php  
			                    $success = $this->session->flashdata('success');
			                    if($success)
			                    {
			                ?>
			                <div class="alert alert-success alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('success'); ?>
			                </div>
			                <?php } ?>
			                

			                <?php  
			                    $warning = $this->session->flashdata('warning');
			                    if($warning)
			                    {
			                ?>
			                 <div class="alert alert-warning alert-dismissable">
			                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			                    <?php echo $this->session->flashdata('warning'); ?>
			                </div>
			                <?php } ?>


			                <div class="row">
			                    <div class="col-md-12">
			                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
			                    </div>
			                </div>
       					 </div>
       					</div>
   					<div class="col_two_third col_last nobottommargin" id="signupView">
						<h3>Don't have an Account? Register Now.</h3>

						 

						<form id="register-form" name="register-form" class="nobottommargin" action="<?php echo base_url();?>auth/signup" method="post">
							<div class="col_half">
								<label for="register-form-name">Name:</label>
								<input type="text" id="register-form-name" required="required" name="register-form-name" value="" class="form-control" />
							</div>

							<div class="col_half col_last">
								<label for="register-form-email">Email Address:</label>
								<input type="email"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required="required" id="register-form-email" name="register-form-email" value="" class="form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_half col_last">
								<label for="register-form-phone">Phone:</label>
								<input type="text"  required="required" id="register-form-phone" name="register-form-phone" value="" class="form-control" />
							</div>

							

							<div class="clear"></div>

							<div class="col_half">
								<label for="register-form-password">Choose Password:</label>
								<input type="password" id="register-form-password" name="register-form-password" required="required" value="" class="form-control" />
							</div>

							<div class="col_half col_last">
								<label for="register-form-repassword">Re-enter Password:</label>
								<input type="password" id="register-form-repassword" name="register-form-repassword" required="required" value="" class="form-control" />
							</div>

							<div class="clear"></div>

							<div class="col_full ">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
							</div>

						</form>

					</div>
				</div>
				<div class="col_two_third col_last nobottommargin" id="otpView">

						<h3>Verify your acccount. Register Now.</h3>

						 

						<form id="register-form" name="register-form" class="nobottommargin" action="<?php echo base_url();?>auth/signup" method="post">
							<div class="col_half">
								<label for="OTP">OTP:</label>
								<input type="text" id="OTP" required="required" name="OTP" value="" class="form-control" />
							</div> 

							<div class="clear"></div>

							<div class="col_full ">
								<button class="button button-3d button-black nomargin" id="register-form-submit" name="register-form-submit" value="register">Register Now</button>
							</div>

						</form>

					</div>

				</div>

			</div>

		</section><!-- #content end -->


		<script type="text/javascript">
			$(document).ready(function() {
				$('#forgotPassword').fadeOut();
				$('#otpView').hide();

				var password = document.getElementById("register-form-password")
   				var confirm_password = document.getElementById("register-form-repassword");

				function validatePassword(){
				  if(password.value != confirm_password.value) {
				    confirm_password.setCustomValidity("Passwords Don't Match");
				  } else {
				    confirm_password.setCustomValidity('');
				  }
				}

			password.onchange = validatePassword;
			confirm_password.onkeyup = validatePassword;

			});

			function forgotPassView() {
				$('#login').fadeOut(1000);
				$('#forgotPassword').fadeIn(2000);
			}
			function loginView() {
				$('#forgotPassword').fadeOut(1000);
				$('#login').fadeIn(2000);
			}
			function otpView(){
				alert();
				$('#signupView').hide();
				$('#otpView').show();
			}


			function singUp(){

					if($('#register-form-repassword').val()=='' || $('#register-form-password').val()=='' || $('#register-form-name').val()=='' || $('#register-form-name').val()=='' || $('#register-form-name').val()==''  )

					if($('#register-form-repassword').val() != $('#register-form-password').val()){
						 
						return false;
					}else{
						 $.ajax({
					      url:"<?php echo base_url();?>auth/signup",
					      data:$('#register-form').serialize(),
					      type:'post',
					      success: function(response){

					        //var jsonObj = JSON.parse(response);
					        
					        console.log(response);
					     }
					 });
				}
			     
			  }

		</script>