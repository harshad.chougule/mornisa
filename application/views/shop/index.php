
<style type="text/css">
	.product-overlay a {
	font-size: large;
	width: 100%;
	}

</style>

<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Shop</h1>
				<span>Start Buying your Favourite Product</span>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active" aria-current="page">Shop</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<!-- Shop
					============================================= -->
					<div id="shop" class="shop product-3 grid-container clearfix" data-layout="fitRows">
						<?php //print_r($products);exit();?>
						<?php foreach ($products as $product) { ?>
							  <?php $viewProductURL = base_url().'product?product_id='.$product['product_id']; //print_r($viewProductURL);exit();?>
						 
						<div class="product clearfix">
							<div class="product-image">
								 
								<?php foreach($product['images'] as $img ){?>
									<a href="<?=$viewProductURL?>"><img src="<?php echo base_url().$img->image_url?>" alt="Checked Short Dress"></a>
								<?php }
								if ($product['is_discount']){
								?>									 
								<div class="sale-flash">Comming soon!</div>
								<?php }?>	
								<div class="product-overlay">
									 
								<?php if($this->session->userdata('isLoggedIn')){ ?>
									<a href="<?=$viewProductURL?>" class="item-quick-view" ><i class="icon-shopping-cart"></i><span> Buy Now!</span></a>
								<?php } else {?>
									<a type="button" href="<?=base_url();?>login" class="item-quick-user"><i class="icon-shopping-cart"></i><span> Buy Now!</span></a>
								<?php }?>

								</div>
							</div>
							<div class="product-desc center">
								<div class="product-title"><h3><a href="<?=$viewProductURL?>"><?=$product['product_name']?></a></h3></div>
								<?php if ($product['is_discount']){?>	
									<div class="product-price"><del>₹<?=$product['old_price']?></del> <ins>₹<?=$product['new_price']?></ins></div>
							 	<?php }else{?>
							 		<div class="product-price">₹<?=$product['new_price']?></div>
						 		<?php } ?>
							</div>
						</div>
						<?php }?>						 
  
					</div><!-- #shop end -->

				</div>

			</div>

		</section><!-- #content end -->