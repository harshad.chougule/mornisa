
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Checkout</h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="<?=base_url()?>shop">Shop</a></li>
					<li class="breadcrumb-item active" aria-current="page">Checkout</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<?php 
				//biiling address
				if(isset($user_info['address'][0])){
					$billing_address_obj = $user_info['address'][0];
					$billing_address=$billing_address_obj->user_address;					 
					$billing_user_pincode=$billing_address_obj->user_pincode;
					$billing_user_city=$billing_address_obj->user_city;
				}else{
					$billing_address="";					 
					$billing_user_pincode="";
					$billing_user_city="";
				}//shipping address
				if(isset($user_info['address'][1])){
					$shipping_address_obj = $user_info['address'][1];
					$shipping_address=$shipping_address_obj->user_address;
					$shipping_user_name=$shipping_address_obj->user_name;
					$shipping_user_pincode=$shipping_address_obj->user_pincode;
					$shipping_user_city=$shipping_address_obj->user_city;
					$shipping_user_phone=$shipping_address_obj->user_phone;
				}else{
					$shipping_address="";
					$shipping_user_name="";
					$shipping_user_pincode="";
					$shipping_user_city="";
					$shipping_user_phone="";
				}
				//print_r($billing_address_obj); exit();?>
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					 
					<div class="clear"></div>

					<div class="row clearfix">
						<div class="col-lg-6">
							<h3>Billing Address</h3>

							 

							<form id="billing-form" name="billing-form" class="nobottommargin" action="placeOrder" method="post">

								<div class="col_full">									
									<label for="billing-form-name">Name:</label>
									<input type="text" id="billing-form-name" readonly="true" name="billing-form-name" class="sm-form-control" value="<?=$user_info[0]['name']?>" />
								</div>

								<div class="col_full">
									<label for="billing-form-address">Address:</label>
									<textarea class="sm-form-control" id="billing-form-address" name="billing-form-address" rows="6" cols="30"><?=$billing_address?></textarea>
								</div>

								<div class="col_half">
									<label for="billing-form-companyname">Pin Code:</label>
									<input type="Number" id="billing-form-pin" name="billing-form-pin" value="<?=$billing_user_pincode?>"   class="sm-form-control" />
								</div>

								<div class="col_half col_last">
									<label for="billing-form-city">City / Town</label>
									<input type="text" id="billing-form-city" name="billing-form-city" value="<?=$billing_user_city?>" class="sm-form-control" />
								</div>

								<div class="col_half">
									<label for="billing-form-email">Email Address:</label>
									<input type="email" id="billing-form-email" value="<?=$user_info[0]['email']?>" readonly="true" name="billing-form-email" value="" class="sm-form-control"  />
								</div>

								<div class="col_half col_last">
									<label for="billing-form-phone">Phone:</label>
									<input type="text" id="billing-form-phone" readonly="true" name="billing-form-phone" value="<?=$user_info[0]['mobile']?>" class="sm-form-control"  />
								</div>
								

								<!--  -->
							<br><br><br>
							 
							<input type="checkbox" name="sameAsAbove" id="sameAsAbove" onchange="sameAsBilling(this)"><b>Same as Billing address</b>
							<br><br><br>
								 
								<h3 class="">Shipping Address</h3>
								<div class="col_half">
									<label for="shipping-form-name">*Name:</label>
									<input type="text" required="" id="shipping-form-name" name="shipping-form-name" value="<?=$user_info[0]['name']?>" class="sm-form-control" />
								</div>

								<div class="col_half col_last">
									<label for="shipping-form-phone">Phone:</label>
									<input type="text" id="shipping-form-phone"  name="shipping-form-phone" value="<?=$user_info[0]['mobile']?>" class="sm-form-control"  />
								</div>
 		

								<div class="clear"></div>
<!-- 
								<div class="col_full">
									<label for="shipping-form-companyname">Company Name:</label>
									<input type="text" id="shipping-form-companyname" name="shipping-form-companyname" value="" class="sm-form-control" />
								</div> -->

								<div class="col_full">
									<label for="shipping-form-address">Address:</label>
									<textarea class="sm-form-control" id="shipping-form-address" name="shipping-form-address"  rows="6" cols="30"> <?=$shipping_address?></textarea>
								</div> 

								<div class="col_half">
									<label for="shipping-form-pin">Pin Code:</label>
									<input type="Number" id="shipping-form-pin" name="shipping-form-pin" value="<?=$shipping_user_pincode?>" class="sm-form-control" />
								</div>

								<div class="col_half col_last">
									<label for="shipping-form-city">City / Town</label>
									<input type="text" id="shipping-form-city" name="shipping-form-city" value="<?=$shipping_user_city?>" class="sm-form-control" />
								</div>

								<div class="col_full">
									<label for="shipping-form-message">Notes <small>*</small></label>
									<textarea class="sm-form-control" id="shipping-form-message" name="shipping-form-message" rows="6" cols="30"></textarea>
								</div>

							 
						</div>
						<div class="col-lg-6">

							<h4>Your Orders</h4>

							<div class="table-responsive">
								<table class="table cart">
									<thead>
										<tr>
											<th class="cart-product-thumbnail">&nbsp;</th>
											<th class="cart-product-name">Product</th>
											<th class="cart-product-quantity">Quantity</th>
											<th class="cart-product-subtotal">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$image_url = base_url().$product_info[0]['images'][0]->image_url;
										$totalPrice =($quantity*$product_info[0]['new_price']);
										?>
										<tr class="cart_item">
											<td class="cart-product-thumbnail">
												<a href="#"><img width="64" height="64" src="<?=$image_url?>" alt="<?=$product_info[0]['product_name']?>"></a>
												<input type="text" hidden="" name="product_name" value="<?=$product_info[0]['product_name']?>">
											</td>

											<td class="cart-product-name">
												<a href="#"><?=$product_info[0]['product_name']?></a>
												<input type="text" hidden="" name="product_id" value="<?=$product_info[0]['product_id']?>">
											</td>

											<td class="cart-product-quantity">
												<div class="quantity clearfix">
													<?=$quantity?> x <?=$product_info[0]['new_price']?>
													<input type="text" hidden="" name="quantity" value="<?=$quantity?>">
												</div>
											</td>

											<td class="cart-product-subtotal">
												<span class="amount">₹<?=$totalPrice?></span>
											</td>
										</tr>
										 
									</tbody>

								</table>
							</div>

							<h4>Cart Totals</h4>

							<div class="table-responsive">
								<table class="table cart">
									<tbody>
										<tr class="cart_item">
											<td class="notopborder cart-product-name">
												<strong>Cart Subtotal</strong>
											</td>

											<td class="notopborder cart-product-name">
												<span class="amount">₹<?=$totalPrice?></span>
											</td>
										</tr>
										<tr class="cart_item">
											<td class="cart-product-name">
												<strong>Shipping</strong>
											</td>

											<td class="cart-product-name">
												<span class="amount">Free Delivery</span>
											</td>
										</tr>
										<tr class="cart_item">
											<td class="cart-product-name">
												<strong>Total</strong>
											</td>

											<td class="cart-product-name">
												<span class="amount color lead"><strong>₹<?=$totalPrice?></strong></span>
											</td>
										</tr>
									</tbody>
								</table>
							</div>

							<div class="accordion clearfix">
								<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Direct Bank Transfer</div>
								<div class="acc_content clearfix">Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</div>

								<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Cheque Payment</div>
								<div class="acc_content clearfix">Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.</div>

								<div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Paypal</div>
								<div class="acc_content clearfix">Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Duis mollis, est non commodo luctus. Aenean lacinia bibendum nulla sed consectetur.</div>
							</div>
							<button type="submit" class="button button-3d fright">Place Order</button>

						
						</div>
						<div class="w-100 bottommargin"></div>
						<div class="col-lg-6">
							
						</div>
						<div class="col-lg-6">
							
							
						</div>
					</div>
				</div>
			</form>
			</div>

		</section><!-- #content end -->
		<script type="text/javascript">
			function sameAsBilling(obj) {
				console.log(obj);
 				 if (obj.checked) {
		            $('#shipping-form-name').val($('#billing-form-name').val());
		            $('#shipping-form-phone').val($('#billing-form-phone').val());
		            $('#shipping-form-address').val($('#billing-form-address').val());
		            $('#shipping-form-pin').val($('#billing-form-pin').val());
		            $('#shipping-form-city').val($('#billing-form-city').val());
		        }else{
		        	$('#shipping-form-name').val('');
		            $('#shipping-form-phone').val('');
		            $('#shipping-form-address').val('');
		            $('#shipping-form-pin').val('');
		            $('#shipping-form-city').val('');
		        }

			}
		</script>