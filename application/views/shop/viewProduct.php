<?php
$product_id = $_GET['product_id'];

		/*$this->db->select('product.*, image.*');
        $this->db->from('tbl_products as product');
        $this->db->join('tbl_product_images as image','image.product_id = product.product_id', 'left');
        $this->db->where('product.product_id', $product_id);
        $this->db->where('product.isDeleted', 0);
        $query = $this->db->get();
        
        $product = $query->result();*/


        $this->db->select('product.*');
        $this->db->from('tbl_products as product');
        $this->db->where('product.product_id', $product_id);
        $this->db->where('product.isDeleted', 0);
        $query = $this->db->get();
        
        $product = $query->result();

        $this->db->select(' image.*');
        $this->db->from('tbl_product_images as image');
        $this->db->where('image.product_id', $product_id);      
        $query = $this->db->get();
        
        $images = $query->result();
      //  print_r($images);exit();

 
?>

<?php if(!$product){
	$this->load->View('includes/404');
 }else{?>





<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1><?=$product[0]->product_name?></h1>
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?=base_url();?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?=base_url();?>shop">Shop</a></li>
					<li class="breadcrumb-item active" aria-current="page">Shop Single</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="single-product">

						<div class="product">

							<div class="col_two_fifth">

								<!-- Product Single - Gallery
								============================================= -->
								<div class="product-image">
									<div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
										<div class="flexslider">
											<div class="slider-wrap" data-lightbox="gallery">
												 <?php foreach($images as $img){?>
												<?php $img_url = base_url().$img->image_url; ?>
												<div class="slide" data-thumb="<?=$img_url?>"><a href="<?=$img_url?>" title="" data-lightbox="gallery-item"><img src="<?=$img_url?>" alt=""></a>
												</div>
												<?php }?>  

												<!-- <div class="slide" data-thumb="<?=base_url()?>uploads/1/3.jpg"><a href="<?=base_url()?>uploads/1/3.jpg" title="Pink Printed Dress - Front View" data-lightbox="gallery-item"><img src="<?=base_url()?>uploads/1/3.jpg" alt="Pink Printed Dress"></a></div>
												<div class="slide" data-thumb="<?=base_url()?>uploads/1/3.jpg"><a href="<?=base_url()?>uploads/1/3.jpg" title="Pink Printed Dress - Side View" data-lightbox="gallery-item"><img src="<?=base_url()?>uploads/1/3.jpg" alt="Pink Printed Dress"></a></div>
												<div class="slide" data-thumb="<?=base_url()?>uploads/1/3.jpg"><a href="<?=base_url()?>uploads/1/3.jpg" title="Pink Printed Dress - Back View" data-lightbox="gallery-item"><img src="<?=base_url()?>uploads/1/3.jpg" alt="Pink Printed Dress"></a></div> -->
												 
											</div>
										</div>
									</div>
									<?php if($product[0]->is_discount){?>
										<div class="sale-flash">Sale!</div>
									<?php }?>
								</div><!-- Product Single - Gallery End -->

							</div>

							<div class="col_two_fifth product-desc">

								<?php if($product[0]->is_discount){?>
								<!-- Product Single - Price
								============================================= -->
								<div class="product-price"><del>₹<?=$product[0]->old_price?></del> <ins>₹<?=$product[0]->new_price?></ins></div><!-- Product Single - Price End -->
								<?php }else{?>
								<div class="product-price"><ins>₹<?=$product[0]->new_price?></ins></div>
								<?php }?>
								<div class="clear"></div>
								<div class="line"></div>

								<!-- Product Single - Quantity & Cart Button
								============================================= -->
								<form class="cart nobottommargin clearfix" method="post" enctype='multipart/form-data' action="<?php echo base_url() ?>checkout">
									<div class="quantity clearfix">
										<input type="button" value="-" class="minus" onclick="minus()">
										<input type="number" step="1" min="1"  name="quantity" value="1" title="Qty" id="count" class="qty" size="4" />
										<input type="button" value="+" class="plus" onclick="plus()">
									</div>
									<input type="text" name="product_id" hidden="" value="<?=$_GET['product_id']?>">
									<?php if($this->session->userdata('isLoggedIn')){ ?>
									<button type="submit" class="add-to-cart button nomargin">Buy Now!</button>	
									<?php } else {?>
									<a type="button" class="add-to-cart button nomargin" href="<?=base_url();?>login">Please Login</a>	
								<?php }?>
								</form><!-- Product Single - Quantity & Cart Button End -->

								<div class="clear"></div>
								<div class="line"></div>

								<!-- Product Single - Short Description
								============================================= -->
								<?=$product[0]->product_description?>
								<!-- Product Single - Short Description End -->

								<!-- Product Single - Meta
								============================================= -->
								<div class="card product-meta">
									<div class="card-body">
										<span itemprop="productID" class="sku_wrapper">SKU: <span class="sku"><?=$product[0]->product_sku?></span></span>
										
									</div>
								</div><!-- Product Single - Meta End -->

								 

							</div>

							<!-- <div class="col_one_fifth col_last">

								<a href="#" title="Brand Logo" class="d-none d-md-block"><img class="image_fade" src="<?php echo base_url(); ?>assets/images/logo-dark.png" alt="Brand Logo"></a>

								<div class="divider divider-center"><i class="icon-circle-blank"></i></div>

								<div class="feature-box fbox-plain fbox-dark fbox-small">
									<div class="fbox-icon">
										<i class="icon-thumbs-up2"></i>
									</div>
									<h3>100% Original</h3>
									<p class="notopmargin">We guarantee you the sale of Original Brands.</p>
								</div>

								<div class="feature-box fbox-plain fbox-dark fbox-small">
									<div class="fbox-icon">
										<i class="icon-credit-cards"></i>
									</div>
									<h3>Payment Options</h3>
									<p class="notopmargin">We accept Visa, MasterCard and American Express.</p>
								</div>

								<div class="feature-box fbox-plain fbox-dark fbox-small">
									<div class="fbox-icon">
										<i class="icon-truck2"></i>
									</div>
									<h3>Free Shipping</h3>
									<p class="notopmargin">Free Delivery to 100+ Locations on orders above $40.</p>
								</div>

								<div class="feature-box fbox-plain fbox-dark fbox-small">
									<div class="fbox-icon">
										<i class="icon-undo"></i>
									</div>
									<h3>30-Days Returns</h3>
									<p class="notopmargin">Return or exchange items purchased within 30 days.</p>
								</div>

							</div> -->

						</div>

					</div>

					<div class="clear"></div><div class="line"></div>

				</div>

			</div>

		</section><!-- #content end -->
		<script type="text/javascript">
			
			var count = 1;
    var countEl = document.getElementById("count");
    function plus(){
        count++;
        countEl.value = count;
    }
    function minus(){
      if (count > 1) {
        count--;
        countEl.value = count;
      }  
    }
</script>
<?php }?>