 
        <!-- Footer
        ============================================= -->
        <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                    <div class="col_two_third">

                        <div class="col_one_third">

                            <div class="widget clearfix">

                                <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" class="footer-logo">

                                <p>We are amongst the prominent Processors and Manufacturer of a comprehensive range of Jaggery (Conical typical Bhelies, Cake, Powder and Syrup) Products.</p>

                                

                            </div>

                        </div>

                        <div class="col_one_third">
                            <div style="background: url('<?php echo base_url(); ?>assets/images/world-map.png') no-repeat center center; background-size: 100%;">
                                    <address>
                                        <strong>Address:</strong><br>
                                        C-46,MIDC,KHADKI,CHALISGAON,<br>
                                        JALGAON - 424101<br>
                                        MAHARSHTRA, 
                                        INDIA.<br>
                                    </address>
                                    <abbr title="Phone Number"><strong>Phone:</strong></abbr> +(91) 9404494440<br>
                                    <abbr title="Fax"><strong>Phone:</strong></abbr> +(91) 9404494477<br>
                                    <abbr title="Email Address"><strong>Email:</strong></abbr> info@mornisa.com
                                </div>

                           

                        </div>

                        <div class="col_one_third col_last">

                             
                              <div class="widget widget_links clearfix">

                                <h4>Menu</h4>

                                <ul>
                                    <li><a href="<?=base_url()?>">Home</a></li>
                                    <li><a href="<?=base_url();?>portfolio">Portfolio</a></li>
                                    <li><a href="<?=base_url();?>gallery">Gallery</a></li>
                                    <li><a href="<?=base_url()?>shop">Shop</a></li>
                                    <li><a href="<?=base_url()?>login/">Login / Singup</a></li>
                                    
                                </ul>

                            </div>

                        </div>

                    </div>

                    <div class="col_one_third col_last">

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                            <div class="row">

                            <div class="fright clearfix">
                            <a href="https://www.facebook.com/mornisajaggery/" target="_blank" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>
   
                            <a href="https://www.instagram.com/mornisajaggery/" class="social-icon si-small si-borderless si-instagram" target="_blank">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>

                            <a href="https://www.youtube.com/channel/UCNNTyd2hzKE134KlUImE8Rg" class="social-icon si-small si-borderless si-youtube" target="_blank">
                                <i class="icon-youtube"></i>
                                <i class="icon-youtube"></i>
                            </a>
                        </div>

                            </div>

                        </div>

                        <div class="widget subscribe-widget clearfix">
                            <h5><strong>We Accept</strong>  <i><img src="<?php echo base_url(); ?>assets/images/footer/mastercard.png" alt="" ></i>
                          <i><img src="<?php echo base_url(); ?>assets/images/footer/paypal2.png" alt="" ></i>  
                           <i><img src="<?php echo base_url(); ?>assets/images/footer/visa.png" alt="" ></i>  
                            </h5>
                            <div class="widget-subscribe-form-result"></div>
                            
                        </div>

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                            <div class="row">

                                

                            </div>

                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyright @2019 Mornisa Bio-organics | All Rights Reserved | Design by Dmag Marketing
                    </div>

                    <div class="col_half col_last tright">
                        

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> info@Mornisa.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +91-11-6541-6369 <span class="middot">&middot;</span> <i class="icon-skype2"></i> MornisaOnSkype
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- Go To Top
    ============================================= -->
    <div id="gotoTop" class="icon-angle-up"></div>

    <!-- External JavaScripts
    ============================================= -->
   
    <script src="<?php echo base_url(); ?>assets/js/plugins.js"></script>

    <!-- Footer Scripts
    ============================================= -->
    <script src="<?php echo base_url(); ?>assets/js/functions.js"></script>
    
    <script type="text/javascript">
        
        var windowURL = window.location.href;
        pageURL = windowURL.substring(0, windowURL.lastIndexOf('/'));
        var x= $('a[href="'+pageURL+'"]');
            x.addClass('current');
            x.parent().addClass('current');
        var y= $('a[href="'+windowURL+'"]');
            y.addClass('current');
            y.parent().addClass('current');
   
    </script>
</body>
</html>