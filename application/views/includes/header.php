<?php //echo '<pre>',print_r($controller."--->".$pagename);
      //print_r($pagename);?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>

    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    

      <!-- Stylesheets
  ============================================= -->
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/swiper.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/magnific-popup.css" type="text/css" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css" type="text/css" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
  </head>

<style type="text/css">

  #header.full-header #primary-menu > ul {
    float: left;
    padding-right: 15px;
    margin-right: 0px;
    border-right: 0px solid #fff;
}
  
  #header, #header-wrap, #logo img {
    height: 100px;
   
    -webkit-transition: height .4s ease, opacity .3s ease;
    -o-transition: height .4s ease, opacity .3s ease;
    transition: height .4s ease, opacity .3s ease;
}
#logo img {
   height: 60px;
   margin-top: 17px;
}
header.transparent-header.full-header.sticky-header #logo img {
    margin-top: 5px;
    height: 50px !important;
}
.dark #header-wrap:not(.not-dark) #primary-menu > ul > li > a{
  color: #444;
}
#content p {
    line-height: 1.8;
    text-align: justify;
}
@media (max-width: 575px){
#primary-menu-trigger {
    left: 0px;
}
}
</style>
<body class="stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Top Bar
    ============================================= -->
    <div id="top-bar" class="d-none d-md-block">

      <div class="container clearfix">

        <div class="col_half nobottommargin">

          <p class="nobottommargin"><strong>Call:</strong> +(91) 940 449 4440 | <strong>Email:</strong> info@mornisa.com</p>

        </div>

        <div class="col_half col_last fright nobottommargin">

          <!-- Top Links
          ============================================= -->
          <div class="top-links">
            <ul>
              <li><a href="https://www.facebook.com/mornisajaggery/" target="_blank" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>               
              </li>
              <li><a href="https://www.instagram.com/mornisajaggery/" class="social-icon si-small si-borderless si-instagram" target="_blank">
                                <i class="icon-instagram"></i>
                                <i class="icon-instagram"></i>
                            </a>
              </li>
              <li><a href="https://www.youtube.com/channel/UCNNTyd2hzKE134KlUImE8Rg" class="social-icon si-small si-borderless si-youtube" target="_blank">
                                <i class="icon-youtube"></i>
                                <i class="icon-youtube"></i>
                            </a>
                
              </li>
              <li>
                <b style="margin-left: 20px">
                  <?php if($this->session->userdata('isLoggedIn')){ ?>
                     Hello, <?=$this->session->userdata('name');?>    
                  <?php } ?>
                </b>
              </li>
            </ul>
          </div><!-- .top-links end -->

        </div>

      </div>

    </div><!-- #top-bar end -->





    <!-- Header
    ============================================= -->
    <header id="header" class="transparent-header full-header" data-sticky-class="not-dark">

      <div id="header-wrap">

        <div class="container clearfix">

          <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

          <!-- Logo
          ============================================= -->
          <div id="logo">
            <a href="<?=base_url()?>" class="standard-logo" data-dark-logo="<?php echo base_url(); ?>assets/images/logo-dark.png"><img src="<?php echo base_url(); ?>assets/images/logo.png" alt="Mornisa Logo"></a>
            <a href="<?=base_url()?>" class="retina-logo" data-dark-logo="<?php echo base_url(); ?>assets/images/logo-dark@2x.png"><img src="<?php echo base_url(); ?>assets/images/logo@2x.png" alt="Mornisa Logo"></a>
          </div><!-- #logo end -->

          <!-- Primary Navigation
          ============================================= -->
          <nav id="primary-menu" class="dark">

            <ul>
              <li class="mega-menu"><a href="<?=base_url();?>"><div>Home</div></a>                 
              </li>
              <li class="mega-menu"><a href="<?=base_url();?>portfolio"><div>Portfolio</div></a>
              </li>
              <li class="mega-menu"><a href="<?=base_url();?>download"><div>Download</div></a>
              </li>
              <li class="mega-menu"><a href="<?=base_url();?>contact"><div>Contact</div></a>
              </li>
              <li class="mega-menu"><a href="<?=base_url();?>gallery"><div>Gallery</div></a>
              </li>
              
              <li class="mega-menu"><a href="<?=base_url();?>shop"><div>buy procuct</div></a>                 
              </li>
              <?php if($this->session->userdata('isLoggedIn')){ ?>
                <li class="mega-menu"><a href="<?=base_url();?>dashboard"><div>My Account</div></a></li>
              <li class="mega-menu"><a href="<?=base_url();?>logout"><div>Logout</div></a>
              </li>
             <?php }else{
              ?>
              <li class="mega-menu"><a href="<?=base_url();?>login"><div>Login/Signup</div></a>
              </li>
            <?php }?>
            </ul>
           
          </nav><!-- #primary-menu end -->

        </div>

      </div>

    </header><!-- #header end -->

     