<style type="text/css">
	.firstSlider{
		top: 120px !important;
	}
</style>

<section id="slider" class="slider-element slider-parallax swiper_wrapper full-screen clearfix">
			<div class="slider-parallax-inner">

				<div class="swiper-container swiper-parent">
					<div class="swiper-wrapper">
						<div class="swiper-slide  " style="background-image: url('<?php echo base_url(); ?>assets/images/slider/swiper/1.jpg'); " >
							<div class="container clearfix">
								<div class="slider-caption firstSlider" >
									<h2 data-caption-animate="fadeInUp" style="font-size: 40px;">We Manufacture Best Jaggery</h2>
									 
								</div>
							</div>
						</div>
						<div class="swiper-slide " style="background-image: url('<?php echo base_url(); ?>assets/images/slider/swiper/banner2.jpg'); background-position: center top;">
							<div class="container clearfix">
								<div class="slider-caption">
									<h2 data-caption-animate="fadeInUp" style="color: #EEE">Jaggery Without Chemicals</h2>
									<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200" style="color: #EEE">The jaggery manufactured by our company is chemical-free with the natural color of dark-brown.</p>
								</div>
							</div>
						</div>
						<div class="swiper-slide " style="background-image: url('<?php echo base_url(); ?>assets/images/slider/swiper/banner3.jpg'); background-position: center top;">
							<div class="container clearfix">
								<div class="slider-caption">
									<h2 data-caption-animate="fadeInUp">Products We Manufacture and Deliver</h2>
									<p class="d-none d-sm-block" data-caption-animate="fadeInUp" data-caption-delay="200">We manufacture and deliver different types of jaggery products like jaggery powder which is completely different from raw sugar in taste, color, and nutrition.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
					<div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
				</div>

				<a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

			</div>
		</section>
		<style type="text/css">
	.portfolio-item .portfolio-image{
		width: 98%;
		margin-top: 10px;
	}
</style>
	<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					<div class="row clearfix">

						<div class="col-xl-7">
							<div class="heading-block topmargin">
								<h1>Welcome to Mornisa.<br></h1>
							</div>
							<p class="lead">Established in the year 2015, We "Mornisa Bio-Organics Pvt. Ltd.," are amongst the prominent Processors and Manufacturer of a comprehensive range of Jaggery (Conical typical Bhelies, Cake, Powder and Syrup) Products.</p>
						</div>

						<div class="col-xl-5">

							<div style="position: relative; margin-bottom: -60px;" class="ohidden" data-height-xl="426" data-height-lg="567" data-height-md="470" data-height-md="287" data-height-xs="183">
								<img src="<?=base_url()?>/assets/images/services/main-f.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="100" alt="Chrome">
								<img src="<?=base_url()?>/assets/images/services/main-f.jpg" style="position: absolute; top: 0; left: 0;" data-animate="fadeInUp" data-delay="400" alt="iPad">
							</div>

						</div>

					</div>
				</div>

				<div class="section nobottommargin">
					<div class="container clear-bottommargin clearfix">

						<div class="heading-block topmargin-lg center">
						<h2>Our Specialties</h2>
						
					</div>


						<div class="row topmargin-sm clearfix">

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-heart2 inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Adulteration-free Jaggery</h4>
								</div>
								<!-- <p>We believe in manufacturing and delivering pure, clean, adulteration-free Jaggery to have more and more satisfied customers throughout the world.</p> -->
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line2-drop inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Production in Hygienic Environment</h4>
								</div>
							<!-- 	<p>We manufacture Jaggery in a very hygienic and clean environment. The Jaggery manufactured here is pure and fit for human consumption.</p> -->
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line2-clock inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>On-time Delivery</h4>
								</div>
								<!-- <p>We value the time of our customers. We have a large manufacturing unit where a bulk production of Jaggery takes place to deliver our products timely.</p> -->
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line-ribbon inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Premium-quality Products</h4>
								</div>
							<!-- 	<p>We guarantee our customers of delivering the premium-quality products of Jaggery which has a different color from raw sugar.</p> -->
							</div>

						</div>

					</div>

					<div class="container clear-bottommargin clearfix">

						<div class="heading-block topmargin-lg center">
						<h2>What We Offer</h2>
						
					</div>


						<div class="row topmargin-sm clearfix">

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-heart2 inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Satisfied Customers</h4>
								</div>
								<p>We provide the best Jaggery which is in its purest form whereby our customers will be thoroughly satisfied.</p>
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line2-drop inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Guaranteed Trust</h4>
								</div>
								<p>We are here to build trust in all the products we offer. This will help us grow leaps and bounds.</p>
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line2-clock inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Great Technology</h4>
								</div>
								<p>We will make jaggery much suitable for human consumption in this era of health consciousness & stress life.</p>
							</div>

							<div class="col-lg-3 bottommargin">
								<i class="i-plain color i-large icon-line-ribbon inline-block" style="margin-bottom: 15px;"></i>
								<div class="heading-block nobottomborder" style="margin-bottom: 15px;">
									<!-- <span class="before-heading">Sub heading.</span> -->
									<h4>Expert Team</h4>
								</div>
								<p>Ours is a very reputed and expert team who do lots of research and development throughout the manufacturing process.</p>
							</div>

						</div>

					</div>
					<div class="container clearfix">
						<div class="heading-block center nomargin">
							<h3>Our Products</h3>
						</div>
				</div>

				</div>




 				

				<div id="portfolio" class="portfolio portfolio-nomargin grid-container portfolio-notitle portfolio-full grid-container clearfix">

					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-1.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-1.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 1</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					 	<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-3.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-3.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 2</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-4.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-4.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 3</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					 
 					
				 	<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-5.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-5.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 4</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-6.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-6.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 5</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					 <article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-7.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-7.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 6</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>

					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-8.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-8.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 7</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>
					<article class="portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="<?=base_url()?>shop">
								<img src="<?=base_url()?>/assets/images/portfolio/4/mornisa-9.png" alt="Open Imagination">
							</a>
							<div class="portfolio-overlay">
								<a href="<?=base_url()?>/assets/images/portfolio/4/mornisa-9.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="<?=base_url()?>shop" class="right-icon"><i class="icon-eye"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="<?=base_url()?>shop">Product 8</a></h3>
							<span><a href="#">Some information </a>, <a href="#">some info</a></span>
						</div>
					</article>
 
 

				</div>


			<div class="clear"></div>












				<div class="container clearfix">

					<div class="heading-block topmargin-lg center">
						<h2>Benefits of Having Pure Jaggery</h2>
						<span class="divcenter">Natural Chemical Free Products Manufactured With Love
</span>
					</div>

					<div class="row bottommargin-sm">

						<div class="col-lg-4 col-md-6 bottommargin">

							<div class="feature-box fbox-right topmargin" data-animate="fadeIn">
								<div class="fbox-icon">
									<a href="#"><i class="icon-health"></i></a>
								</div>
								<h3>Improves Digestion</h3>
								<p>Pure Jaggery is always beneficial for stomach and digestive process. Having chemical-free jaggery after a meal activates the digestive enzymes and improves overall digestion.</p>
							</div>

							<div class="feature-box fbox-right topmargin" data-animate="fadeIn" data-delay="200">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line-paper"></i></a>
								</div>
								<h3>Beneficial in Anemia</h3>
								<p>Natural jaggery has numerous essential nutrients like Iron which is required to treat anemia. When jaggery is consumed regularly, it increases the level of hemoglobin and improving the blood circulation in the body.</p>
							</div>

							<div class="feature-box fbox-right topmargin" data-animate="fadeIn" data-delay="400">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line2-energy"></i></a>
								</div>
								<h3>Energy Booster</h3>
								<p>Jaggery is also considered as an energy booster that releases a lot of energy when consumed regularly. As carbohydrates are present in jaggery, it provides freshness to the body for long hours.</p>
							</div>

						</div>

						<div class="col-lg-4 d-md-none d-lg-block bottommargin center">
							<img src="<?=base_url()?>/assets/images/services/Jaggery.png" alt="iphone 2">
							<img src="<?=base_url()?>/assets/images/services/Jaggery.png" alt="iphone 2">
						</div>

						<div class="col-lg-4 col-md-6 bottommargin">

							<div class="feature-box topmargin" data-animate="fadeIn">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line2-graph"></i></a>
								</div>
								<h3>Improves Immunity</h3>
								<p>With essential nutrients, jaggery is also enriched with dietary fibers in large quantity and it treats indigestion and reduces the bowel syndrome risk. This improves immunity.</p>
							</div>

							<div class="feature-box topmargin" data-animate="fadeIn" data-delay="200">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line-check"></i></a>
								</div>
								<h3>Overcome Obesity</h3>
								<p>The presence of potassium in high-quantity in jaggery makes it a key contributor to overcoming obesity. Potassium helps in reducing bloating and water retention to achieve success in weight loss.</p>
							</div>

							<div class="feature-box topmargin" data-animate="fadeIn" data-delay="400">
								<div class="fbox-icon">
									<a href="#"><i class="icon-line2-star"></i></a>
								</div>
								<h3>Treats Common Ailments</h3>
								<p>Many common ailments can easily be treated with the help of jaggery. Jaggery has many medicinal properties which make it a perfect alternative for treating asthma and other respiratory issues.</p>
							</div>

						</div>

					</div>

				</div>

				<div class="row clearfix common-height">

					<div class="col-lg-6 center col-padding" style="background: url('<?=base_url()?>/assets/images/services/jaggery-sugar.jpg') center center no-repeat; background-size: cover;">
						<div>&nbsp;</div>
					</div>

					<div class="col-lg-6 center col-padding" style="background-color: #F5F5F5;">
						<div>
							<div class="heading-block nobottomborder">
								<!-- <span class="before-heading color">Some sub heading.</span>
								<h3>About Mornisa.</h3> -->
							</div>

							<div class="center bottommargin">
								<a href="https://www.youtube.com/watch?v=bxcX0WcOCv8" data-lightbox="iframe" style="position: relative;">
									<img src="<?=base_url()?>/assets/images/services/Jaggery-Powder.png" alt="Video">
									<!-- <span class="i-overlay nobg"><img src="<?=base_url()?>/assets/images/icons/video-play.png" alt="Play"></span> -->
								</a>
							</div>
							<!-- <p class="lead nobottommargin">We have a large manufacturing unit where a large number of employees work in order to deliver premium quality Jaggery to our customers in bulk.</p> -->
						</div>
					</div>

				</div>

				<div class="row clearfix bottommargin-lg common-height">

					<div class="col-lg-3 col-md-6 dark center col-padding" style="background-color: #a2c622;">
						<div>
							<i class="i-plain i-xlarge divcenter icon-line2-directions"></i>
							<div class="counter counter-lined"><span data-from="100" data-to="10" data-refresh-interval="50" data-speed="2000"></span></div>
							<h5>Benefits</h5>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 dark center col-padding" style="background-color: #73b92c;">
						<div>
							<i class="i-plain i-xlarge divcenter icon-line2-graph"></i>
							<div class="counter counter-lined"><span data-from="3000" data-to="1" data-refresh-interval="100" data-speed="2500"></span></div>
							<h5>Branches</h5>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 dark center col-padding" style="background-color: #50a939;">
						<div>
							<i class="i-plain i-xlarge divcenter icon-users2"></i>
							<div class="counter counter-lined"><span data-from="10" data-to="120" data-refresh-interval="25" data-speed="3500"></span></div>
							<h5>Employees</h5>
						</div>
					</div>

					<div class="col-lg-3 col-md-6 dark center col-padding" style="background-color: #499c44;">
						<div>
							<i class="i-plain i-xlarge divcenter icon-home"></i>
							<div class="counter counter-lined"><span data-from="60" data-to="1000" data-refresh-interval="30" data-speed="2700"></span></div>
							<h5>Families</h5>
						</div>
					</div>

				</div>

				 



				<div class="clear"></div>
 

			 
				 <div class="container clearfix">

					<div class="col_two_third nobottommargin">

						<div class="fancy-title title-border">
							<h4>Customer review</h4>
						</div>

						<div class="col_half nobottommargin">
							<div class="ipost clearfix">
								 
									 


							<div class="center bottommargin">
								<a href="https://www.youtube.com/watch?v=bxcX0WcOCv8" data-lightbox="iframe" style="position: relative;">
									<img src="<?=base_url()?>/assets/images/client/v2.png" alt="Video">
									<span class="i-overlay nobg"><img src="<?=base_url()?>/assets/images/icons/video-play.png" alt="Play"></span>
								</a>
							</div>
								 
								<div class="entry-title">
									<h3><a href="https://www.youtube.com/watch?v=bxcX0WcOCv8" data-lightbox="iframe" style="position: relative;">Mr. Jon Xaris sharing there view on our products</a></h3>
								</div>
								 
							</div>
						</div>

						<div class="col_half col_last nobottommargin">
							<div class="ipost clearfix">
								<div class="center bottommargin">
								<a href="https://www.youtube.com/watch?v=bxcX0WcOCv8" data-lightbox="iframe" style="position: relative;">
									<img src="<?=base_url()?>/assets/images/client/v1.png" alt="Video">
									<span class="i-overlay nobg"><img src="<?=base_url()?>/assets/images/icons/video-play.png" alt="Play"></span>
								</a>
							</div> 
								<div class="entry-title">
									<h3><a href="https://www.youtube.com/watch?v=bxcX0WcOCv8" data-lightbox="iframe" style="position: relative;">Product review by the user1 from City 1</a></h3>
								</div>
								 
							</div>
						</div>

						<div class="clear"></div>

					</div>

					<div class="col_one_third nobottommargin col_last">

						<div class="fancy-title title-border">
							<h4>Client Testimonials</h4>
						</div>

						<div class="fslider testimonial nopadding noborder noshadow" data-animation="slide" data-arrows="false">
							<div class="flexslider">
								<div class="slider-wrap">
									<div class="slide">
										<div class="testi-image">
											<a href="#"><img src="<?php echo base_url(); ?>assets/images/client/1.png" alt="Customer Testimonails"></a>
										</div>
										<div class="testi-content">
											<p>I wish to insist people on buying only Mornisa Jaggery as its natural products are worth every penny. I am extremely delighted to have tried and tested their products which are the best.</p>
											<div class="testi-meta">
												COLLIS TA'EED 
												<span>Mumbai.</span>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="testi-image">
											<a href="#"><img src="<?php echo base_url(); ?>assets/images/client/1.png" alt="Customer Testimonails"></a>
										</div>
										<div class="testi-content">
											<p>I normally don't try any new products as it always creates a doubt, but I must say after trying jaggery manufactured by Mornisa, I am thoroughly satisfied and recommend everyone to use it for life.</p>
											<div class="testi-meta">
												JOHN DOE
												<span>Pune.</span>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="testi-image">
											<a href="#"><img src="<?php echo base_url(); ?>assets/images/client/1.png" alt="Customer Testimonails"></a>
										</div>
										<div class="testi-content">
											<p>The taste of Mornisa Jaggery is very very pure and we are so happy with it. Till date, we didn't get such rich taste of jaggery. It feels very natural and blends well with everything.</p>
											<div class="testi-meta">
												STEVE JOBS 
												<span>Nashik.</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="clear"></div>

					<!-- 	<div class="well topmargin ohidden">

							<h4>Opening Hours</h4>

							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit reprehenderit voluptates.</p>

							<ul class="iconlist nobottommargin">
								<li><i class="icon-time color"></i> <strong>Mondays-Fridays:</strong> 10AM to 7PM</li>
								<li><i class="icon-time color"></i> <strong>Saturdays:</strong> 11AM to 3PM</li>
								<li><i class="icon-time text-danger"></i> <strong>Sundays:</strong> Closed</li>
							</ul>

							<i class="icon-time bgicon"></i>

						</div> -->

					</div>

					<div class="clear"></div>

				</div>

				 
			

			</div>

		</section><!-- #content end -->

		<script type="text/javascript">
			$(document).ready(function(){ 
				setTimeout(function() { 
			    $('.slider-arrow-right').click(); 
			    },3000);
			});
   

		</script>
		<!--  -->